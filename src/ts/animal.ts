var message: string = "Hello World";
console.log(message);


class Greeting {
    greet(): void {
        console.log("Hello World!!!")
    }
}
var obj = new Greeting();
obj.greet();


class People {
    id: number = 0;
    name: string = '';
    gender: string = '';
    full_name: string = 'Nora Koso';

    setPeople(id: number, name: string, gender: string, full_name: string) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.full_name = full_name;
    }

    getId(): number {
        return this.id;
    }
    getName(): string {
        return this.name;
    }
    getGender(): string {
        return this.gender;
    }
    getFullName(): string {
        return this.full_name;
    }
}

var people = new People();
people.setPeople(1, "Nora", "Female", "Nora Kosa");
console.log("People ", people.getFullName());
console.log("Number ", Number.MAX_VALUE);
console.log("test typescript...");

console.log("1. Array one dimension Testing ");
var arr: string[] = new Array();
arr = ["1", "2", "3", "4", "5", "6", "7", "8"];
console.log("one , ", arr);
console.log('arr.length : ', arr.length);
console.log("2. Array Two dimension Testing ");
var arr1: string[][] = new Array();
var multi: number[][] = new Array();
multi = [[1, 2, 3], [23, 24, 25]]
console.log(multi[0][0])
console.log('arr : ', arr1);

var arr2 = ["1", 2, 'A']
arr2.push("Noly time");
rmArray(2);
console.log('arr2: ', arr2);

function rmArray(item:any):void{
    for(let i =0; i<arr2.length;i++){
        if(arr2[i]==item){
            arr2.splice(i, (i+1));
            continue;
        }
    }
}




class Car {
    //field 
    engine: string;

    //constructor 
    constructor(engine: string) {
        this.engine = engine
    }

    //function 
    disp(): void {
        console.log("Engine is  :   " + this.engine)
    }
}

var car = new Car("Mobile Legend..");
car.disp();



