"use strict";
var message = "Hello World";
console.log(message);
var Greeting = /** @class */ (function () {
    function Greeting() {
    }
    Greeting.prototype.greet = function () {
        console.log("Hello World!!!");
    };
    return Greeting;
}());
var obj = new Greeting();
obj.greet();
var People = /** @class */ (function () {
    function People() {
        this.id = 0;
        this.name = '';
        this.gender = '';
        this.full_name = 'Nora Koso';
    }
    People.prototype.setPeople = function (id, name, gender, full_name) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.full_name = full_name;
    };
    People.prototype.getId = function () {
        return this.id;
    };
    People.prototype.getName = function () {
        return this.name;
    };
    People.prototype.getGender = function () {
        return this.gender;
    };
    People.prototype.getFullName = function () {
        return this.full_name;
    };
    return People;
}());
var people = new People();
people.setPeople(1, "Nora", "Female", "Nora Kosa");
console.log("People ", people.getFullName());
console.log("Number ", Number.MAX_VALUE);
console.log("test typescript...");
console.log("1. Array one dimension Testing ");
var arr = new Array();
arr = ["1", "2", "3", "4", "5", "6", "7", "8"];
console.log("one , ", arr);
console.log('arr.length : ', arr.length);
console.log("2. Array Two dimension Testing ");
var arr1 = new Array();
var multi = new Array();
multi = [[1, 2, 3], [23, 24, 25]];
console.log(multi[0][0]);
console.log('arr : ', arr1);
var arr2 = ["1", 2, 'A'];
arr2.push("Noly time");
rmArray(2);
console.log('arr2: ', arr2);
function rmArray(item) {
    for (var i = 0; i < arr2.length; i++) {
        if (arr2[i] == item) {
            arr2.splice(i, (i + 1));
            continue;
        }
    }
}
var Car = /** @class */ (function () {
    //constructor 
    function Car(engine) {
        this.engine = engine;
    }
    //function 
    Car.prototype.disp = function () {
        console.log("Engine is  :   " + this.engine);
    };
    return Car;
}());
var car = new Car("Mobile Legend..");
car.disp();
