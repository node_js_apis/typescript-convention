"use strict";
/**
 * <pre>
 * EMPLINFO PROJECT
 * @COPYRIGHT (c) 2009-2012 WebCash, Inc. All Right Reserved.
 *
 * @File Name      		: empl_1001_01.js
 * @File path    	 	: EMPLINFO_PT_STATIC/web/js/emplinfo/empl
 * @author       	 	: admin ( 1111 )
 * @Description    		: 직원목록
 * @History      	 	: 20140717062545, admin
 * </pre>
 **/
var _domain = document.location.href.split("/").slice(0, 3).join("/");
var _datTrcoList = [];
var MOBILE_STATUS = fn_loadEmplVisible();
var SearchList = /** @class */ (function () {
    function SearchList() {
        this.PAGE_PER_CNT = 10; //한페이지당 표시 목록 수
        this.PAGE_NO = 1; //현재페이지 번호
        this.INQY_DSNC = "1"; //1:전체,2:기본그룹(부서),3:사용자그룹,4:휴지통,5:즐겨찾기,6:미지정부서조회,7:사업장에 속한 미지정 부서조회
        this.GRP_CD = ""; //그룹검색
        this.SRCH_WORD = ""; //직원명 검색
        this.TOTL_PAGE_NCNT = 1; //총페이지수
        this.BSNN_NO = ""; //사업자번호 (INQY_DSNC 값이 7일때 사용)
    }
    //const isAdmin			= false;
    //TEST
    SearchList.prototype.setPagePerCnt = function (pagepercnt) {
        this.PAGE_PER_CNT = pagepercnt;
    };
    SearchList.prototype.getPagePerCnt = function () {
        return this.PAGE_PER_CNT;
    };
    ;
    SearchList.prototype.setPageNo = function (pageno) {
        this.PAGE_NO = pageno;
    };
    ;
    SearchList.prototype.getPageNo = function () {
        return this.PAGE_NO;
    };
    ;
    SearchList.prototype.setTotlPageNcnt = function (totlpagencnt) {
        this.TOTL_PAGE_NCNT = totlpagencnt;
    };
    ;
    SearchList.prototype.getTotlPageNcnt = function () {
        return this.TOTL_PAGE_NCNT;
    };
    ;
    SearchList.prototype.setInqyDsnc = function (inqydsnc) {
        this.INQY_DSNC = inqydsnc;
    };
    ;
    SearchList.prototype.getInqyDsnc = function () {
        return this.INQY_DSNC;
    };
    ;
    SearchList.prototype.setGrpCd = function (grpcd) { this.GRP_CD = grpcd; };
    ;
    SearchList.prototype.getGrpCd = function () { return this.GRP_CD; };
    ;
    SearchList.prototype.setSrchWord = function (srchword) { this.SRCH_WORD = srchword; };
    ;
    SearchList.prototype.getSrchWord = function () { return this.SRCH_WORD; };
    ;
    SearchList.prototype.setBsnnNo = function (bsnnno) { this.BSNN_NO = bsnnno; };
    ;
    SearchList.prototype.getBsnnNo = function () { return this.BSNN_NO; };
    ;
    SearchList.prototype.setSearchJson = function (queryJson) {
        this.PAGE_PER_CNT = queryJson.PAGE_PER_CNT;
        this.PAGE_NO = queryJson.PAGE_NO;
        this.INQY_DSNC = queryJson.INQY_DSNC;
        this.GRP_CD = queryJson.GRP_CD;
        this.SRCH_WORD = queryJson.SRCH_WORD;
    };
    ;
    SearchList.prototype.getSearchString = function () {
        return "PAGE_PER_CNT=" + this.PAGE_PER_CNT + "&PAGE_NO=" + this.PAGE_NO + "&INQY_DSNC=" + this.INQY_DSNC + "&GRP_CD=" + this.GRP_CD; //+"&TRCO_CMNM="+TRCO_CMNM;
    };
    ;
    return SearchList;
}());
;
var _searchList = new SearchList();
var _loding;
var _tmpCurGrpCd = "";
var _tmpCurGrpNm = "";
var _tmpCurGrpIcon = "";
var VType = "";
var erlaubt = false;
var lasty;
var Clk_Vtp;
var thumb_img_size;
var thumb_per_page = 30;
var viewType = 0;
var thumbTotal;
var _INQ_JOIN_DV = ""; //조회조건 가입여부('' : 전체, 'Y' : 가입, 'N' : 미가입)
var _SUBMIT_YN = "Y"; //즐겨찾기의 경우 재실행 방지, Y일 경우 실행 가능 
var tree_dvsn_cd = "";
var TREE_LOAD = false;
var on_loadVtyp3 = -1;
//daravuth on 07.27.2016
//#20016, 12122019 1:06PM
var _emplVisibleDat = {};
var excludeEmpl = false;
new (Jex.extend({
    onload: function () {
        _emplVisibleDat = fn_loadEmplVisible();
        // _this = this;
        //--- todo onload start ---//
        // load all default configuration
        //loadDefaultConfig(Clk_Vtp);
        _loding = jex.plugin.get("JEX_LODING");
        //ViewType defual removeClass on update20160401
        $(".btnbx_r a").removeClass("on");
        $("#th_btn_moreview").hide();
        //check if have no company
        if (top._use_intt_id == "N") {
            $("#ifr_content").on("load", function () {
                $("#div_content").hide(); //직원 목록
                $("#ifr_content").show();
                if ($(this).contents().find("body").height() != 0) {
                    $(this).css("height", (Number($(this).contents().find("body").height()) + 50) + "px");
                }
                parent.ifrMainResize("", 0); // 20160721 borey
            });
            $("#ifr_content").attr("src", "empl_1010_01.act");
            return;
        }
        //관리자일 경우 등록버튼 보임
        /*if($("#str_cnts_msgr_yn").attr("value") == "Y"){
            $("#addEmplBtn2").show();
            $("#popEmplBtn").show(); //직원초대
            if(parent.getBannerOpen()){ //배너 Open
                //미가입 직원 있는지 확인
                if(parent.getUnjoinInqYn() != "Y"){
                    const jexAjax = jex.createAjaxUtil("empl_1001_01_r001");
                    jexAjax.setErrTrx(false);
                    jexAjax.execute(function(dat) {
                        if(dat.COMMON_HEAD.ERROR){
                            _loding.stop();
                            parent.showPrintInfoMsg("",dat.COMMON_HEAD.MESSAGE);
                            return;
                        }
                        parent.setUnjoinInqYn();
                        if( dat.YN == "Y" ){ //미가입 직원 있으면 배너 show
                                //$("#rcmd_bnr").show();
                        } else {
                            parent.setBannerClose();
                            //$("#rcmd_bnr").hide();
                        }
                    });
                    
                } else {
                    $("#rcmd_bnr").show();
                }
            }
            
            const tooltipOpen = parent.getTooltipOpen();
            //직원초대 안내 tooltip
            if($.cookie("COOKIE_RCMD_TOOLTIP") != "close" && tooltipOpen){
                $("#rcmd_tip").show();
            }
        }*/
        if (_searchList.getPagePerCnt() == 10) {
            $("#thumb_viewwrap").hide();
            _searchList.setPagePerCnt(30);
            $("#thumb_viewwrap").show();
        }
        //상단 검색(enter key)
        $("#frmSearch").submit(function () {
            parent.searchWord(excludeEmpl);
        });
        //Close 상태일경우만 알림내역 보임
        if ($("#str_ptl_stts").attr("value") == "C") {
            parent.ntfyShow();
            //알림내역조회(Close상태일 경우만 알림내역 표시)
            if (parent._isTimer == false) {
                parent._isTimer = true;
                parent.ntfyCheck();
            }
        }
        else {
            parent.ntfyHide();
        }
        //페이지 목록 수 설정
        if ($.cookie('COOKIE_PAGE_PER_CNT_TRCO')) {
            _searchList.setPagePerCnt($.cookie('COOKIE_PAGE_PER_CNT_TRCO'));
            $("#pagingPopBtn a:eq(0)").html("<span><em class=\"icon_list\"></em>" + _searchList.getPagePerCnt() + "</span>");
        }
        drawProfile(); //프로필정보 표시
        // #5370
        $("#div_content").hide();
        /*
        const jexAjax = jex.createAjaxUtil("empl_2001_01_l001");
        jexAjax.setErrTrx(false);
        jexAjax.execute(function(dat) {
            const respData = JSON.parse(dat.RESP_DATA);
            if(respData && respData.length == 0 && $("#str_cnts_msgr_yn").attr("value") == "Y"){
                _INQ_JOIN_DV = "";
                //drawTrcoList();
                drawBscGrpList();
                drawUserGrpList();
//				if(top._moveUseCd == "Y" && $("#str_cnts_msgr_yn").val() == "Y"){
//					_loding.start();
//					$("#ifr_content").attr("src","empl_0003_01.act");
//				}else{
//					parent.ifr_main.openIfrContent("empl_1011_01.act");
//				}
                //top.$("#status").val("Y");
//				return
            }else{
                //상단 검색바에서 검색한 경우
                const queryString = jex.getQString();
                if(queryString.searchTxt){
                    _INQ_JOIN_DV = "";
                    _searchList.setSrchWord(queryString.searchTxt);
                    drawTrcoList();				//직원 목록 표시
                    drawBscGrpList();			//기본그룹 목록 표시
                    drawUserGrpList();			//사용자그룹 목록 표시
                }
                //그외
                else{
                    _INQ_JOIN_DV = "";
                    drawTrcoList();				//직원 목록 표시
                    drawBscGrpList();			//기본그룹 목록 표시
                    drawUserGrpList();			//사용자그룹 목록 표시
                }
                if(top._moveUseCd == "Y" && $("#str_cnts_msgr_yn").val() == "Y"){
                    _loding.start();
                    $("#ifr_content").attr("src","empl_0003_01.act");
                }else{
                    $("#div_content").show();
                }
                
            }
        })*/
        var queryString = jex.getQString();
        if (queryString.searchTxt) {
            _INQ_JOIN_DV = "";
            _searchList.setSrchWord(queryString.searchTxt);
            drawTrcoList(); //직원 목록 표시
            drawBscGrpList(); //기본그룹 목록 표시
            drawUserGrpList(); //사용자그룹 목록 표시		
        }
        //그외
        else {
            _INQ_JOIN_DV = "";
            drawTrcoList(); //직원 목록 표시
            drawBscGrpList(); //기본그룹 목록 표시
            drawUserGrpList(); //사용자그룹 목록 표시		
        }
        $("#div_content").show();
        // /#5370
        $("#ifr_content").on("load", function () {
            $("#div_content").hide(); //직원 목록
            $("#ifr_content").show();
            if ($(this).contents().find("body").height() != 0) {
                $(this).css("height", (Number($(this).contents().find("body").height()) + 20) + "px");
                $(this).contents().find("body").css("background", "white");
            }
            parent.ifrMainResize("", 0); // 20160721 borey
            _loding.stop();
        });
        //부서영역 크기조절
        $(".btn_drag").mousedown(function (e) {
            erlaubt = true;
            lasty = e.pageY;
            return false;
        });
        $(document).mouseup(function (e) {
            erlaubt = false;
            return false;
        });
        $(document).mousemove(function (e) {
            if (erlaubt == false)
                return;
            var change = -(lasty - e.pageY);
            $(".scroll_tree_box").stop(true, true).animate({ 'height': '+=' + change });
            lasty = e.pageY;
            parent.ifrMainResize("", 0); // 20160721 borey
            return false;
        });
        //		window.setTimeout(function(){
        //dsvn_use_cd_view();			
        //		}, 500);
        /*disableSelection(document.body);*/
        //Fix checkbox
        $(document).delegate("#tbTbody_TRCO_LIST .checkbox", "click", function () {
            if ($("#tbTbody_TRCO_LIST .checkbox").not("[disabled]").length == $("#tbTbody_TRCO_LIST .checkbox:checked").length) {
                $("#allCheck").prop("checked", true);
            }
            else {
                $("#allCheck").prop("checked", false);
            }
        });
    }, event: function () {
        //--- define event action ---//
        //---------------------------
        // Left (왼쪽메뉴그룹)
        //---------------------------
        /*this.addEvent("#case1","click",function(){
            parent.moveUseCd();
            parent.removeViewMore();
        });
        this.addEvent("#case2","click",function(){
            parent.removeViewMore();
            parent.moveDvsn();
        });
        this.addEvent("#case3","click",function(){
            parent.removeViewMore();
            if ($("#str_cnts_msgr_yn").attr("value") == "Y")
                parent.ifr_main.openIfrContent("empl_1005_01.act");
            else {
                alert("직원일괄등록은 회사 관리자만 이용 가능합니다.");
                return;
            }
        });
        this.addEvent("#case4","click",function(){
            parent.bizJs.smartInfo('http://sportal.dev.weplatform.co.kr:19990/', '102');
        });*/
        //직원초대
        this.addEvent("#popEmplBtn", "click", function () {
            parent.setTooltipClose();
            $("#rcmd_tip").hide();
            getSelectInfo(false);
        });
        //직원초대 배너
        this.addEvent("#rcmd_bn_btn", "click", function () {
            getSelectInfo(true);
        });
        this.addEvent("#profileEditBtn", "click", function () {
            var url = "empl_1002_01.act?"
                + "&EMPL_IDNT_ID=" + $("#str_empl_idnt_id").val()
                + "&DEL_YN=N"
                + "&MY_PROFILE=Y"
                + "&_tmp="
                + "&CLPH_NO_OPPB_YN=" + _emplVisibleDat.CLPH_NO_OPPB_YN
                + "&EML_OPPB_YN=" + _emplVisibleDat.EML_OPPB_YN;
            parent.openEmplPopContent(url);
        });
        //회사 전체조회
        this.addEvent("#totEmplBtnNew", "click", function () {
            $(".view_listwrap").css("display", "block");
            $(".tab_schbx").css("display", "none");
            //daravuth 07.27.2016
            thumb_per_page = 30; // 07.28.2016
            excludeEmpl = false;
            $("#texSearch").val("");
            _tmpCurGrpNm = $(this).html();
            $("#contentTitle h1").html(_tmpCurGrpNm);
            var _chkV_type = $("#VIEW_TYPE_CD").val();
            _searchList.setInqyDsnc("1");
            _searchList.setPageNo(1);
            //test chen
            var Df_SetupV = $("#Df_SetupV").val();
            if (Df_SetupV == "true") {
                //modify 20160722 chen 
                if (_chkV_type == "1") {
                    $(".btnbx_r a.btn_thumb").click(); //chen 20160728
                }
                else if (_chkV_type == "2") {
                    $(".btnbx_r a.btn_list").click(); //chen 20160728
                }
                else if (_chkV_type == "3") {
                    $(".btnbx_r a.btn_org").click(); //chen 20160728
                }
                $("#Df_SetupV").val("false");
            }
            if (_chkV_type == "1") {
                _searchList.setPagePerCnt(30);
            }
            else if (_chkV_type == "2") {
                _searchList.setPagePerCnt(10);
                $("#combo_row").text(_searchList.getPagePerCnt());
                $("#pagingPopBtn a:eq(0)").html("<span><em class=\"icon_list\" style=\"display:none;\"></em>" + _searchList.getPagePerCnt() + "</span>");
            }
            else if (_chkV_type == "3") {
            }
            //			_searchList.setPagePerCnt(30);
            _searchList.setSrchWord("");
            parent.setSearchTextDefault();
            _INQ_JOIN_DV = "";
            drawTrcoList(); //직원 목록 표시
            drawBscGrpList(); //기본그룹 목록 표시
            drawUserGrpList(); //사용자그룹 목록 표시		
            leftMenuAllOff();
            $("#totEmplBtn").removeClass("on").addClass("on");
            $("#div_content").show();
            $("#ifr_content").hide();
        });
        //나의그룹 전체 조회
        this.addEvent("#userGrpDefaultNew", "click", function () {
            $(".view_listwrap").css("display", "block");
            $(".tab_schbx").css("display", "none");
            //daravuth 07.27.2016
            excludeEmpl = false;
            //thumb_per_page = 30; // 07.28.2016
            $("#texSearch").val("");
            _tmpCurGrpNm = $(this).html();
            $("#contentTitle h1").html(_tmpCurGrpNm);
            closeIfrContent2();
            leftMenuAllOff();
            $(this).removeClass("on").addClass("on");
            _searchList.setInqyDsnc("3");
            _searchList.setPageNo(1);
            _searchList.setGrpCd("");
            _searchList.setSrchWord("");
            parent.setSearchTextDefault();
            //test chen
            var _chkV_type = $("#VIEW_TYPE_CD").val();
            var Df_SetupV = $("#Df_SetupV").val();
            if (Df_SetupV == "true") {
                //modify 20160722 chen 
                if (_chkV_type == "1") {
                    $(".btnbx_r a.btn_thumb").click(); //chen 20160728
                }
                else if (_chkV_type == "2") {
                    $(".btnbx_r a.btn_list").click(); //chen 20160728
                }
                else if (_chkV_type == "3") {
                    $(".btnbx_r a.btn_org").click(); //chen 20160728
                }
                $("#Df_SetupV").val("false");
            }
            //by chen 20160729
            if (_chkV_type == "1") {
                _searchList.setPagePerCnt(30);
            }
            else if (_chkV_type == "2") {
                _searchList.setPagePerCnt(10);
                $("#combo_row").text(_searchList.getPagePerCnt());
                $("#pagingPopBtn a:eq(0)").html("<span><em class=\"icon_list\" style=\"display:none;\"></em>" + _searchList.getPagePerCnt() + "</span>");
            }
            else if (_chkV_type == "3") {
            }
            _INQ_JOIN_DV = "";
            if (_chkV_type == "3") {
                //				drawTrcoList();				//직원 목록 표시
                drawBscGrpList(); //기본그룹 목록 표시
                drawUserGrpList(); //사용자그룹 목록 표시
            }
            else {
                drawTrcoList(); //직원 목록 표시
                drawBscGrpList(); //기본그룹 목록 표시
                drawUserGrpList(); //사용자그룹 목록 표시
            }
            //loadDefaultConfig();
        });
        //즐겨찾기 선택시 
        this.addEvent("#userGrpDefault dd", "click", function () {
            _tmpCurGrpNm = $(this).find("span:eq(2)").html();
            $("#contentTitle h1").html(_tmpCurGrpNm);
            closeIfrContent2();
            leftMenuAllOff();
            $(this).removeClass("on").addClass("on");
            _searchList.setInqyDsnc("5");
            _searchList.setPageNo(1);
            _searchList.setGrpCd("");
            _searchList.setSrchWord("");
            parent.setSearchTextDefault();
            //test chen
            var _chkV_type = $("#VIEW_TYPE_CD").val();
            var Df_SetupV = $("#Df_SetupV").val();
            if (Df_SetupV == "true") {
                //modify 20160722 chen 
                if (_chkV_type == "1") {
                    $(".btnbx_r a.btn_thumb").click(); //chen 20160728
                }
                else if (_chkV_type == "2") {
                    $(".btnbx_r a.btn_list").click(); //chen 20160728
                }
                else if (_chkV_type == "3") {
                    $(".btnbx_r a.btn_org").click(); //chen 20160728
                }
                $("#Df_SetupV").val("false");
            }
            _INQ_JOIN_DV = "";
            //by chen 20160729
            if (_chkV_type == "1") {
                _searchList.setPagePerCnt(30);
            }
            else if (_chkV_type == "2") {
                _searchList.setPagePerCnt(10);
                $("#combo_row").text(_searchList.getPagePerCnt());
                $("#pagingPopBtn a:eq(0)").html("<span><em class=\"icon_list\" style=\"display:none;\"></em>" + _searchList.getPagePerCnt() + "</span>");
            }
            else if (_chkV_type == "3") {
            }
            if (_chkV_type == "3") {
                //drawTrcoList();				//직원 목록 표시
                drawBscGrpList(); //기본그룹 목록 표시
                drawUserGrpList(); //사용자그룹 목록 표시	
            }
            else {
                drawTrcoList(); //직원 목록 표시
                drawBscGrpList(); //기본그룹 목록 표시
                drawUserGrpList(); //사용자그룹 목록 표시	
            }
            //loadDefaultConfig();
        });
        //왼쪽 메뉴 사용자그룹 선택시 
        $("#userGrpList dd a").live("click", function () {
            $(".view_listwrap").css("display", "block");
            $(".tab_schbx").css("display", "none");
            //daravuth 07.27.2016
            excludeEmpl = false;
            //thumb_per_page = 30; // 07.28.2016
            $("form#frmSearch").find("input").val("");
            $("form#frmSearch").find("input").blur();
            //그룹명 수정에서 v,x 버튼 눌렀을 경우는 실행안되게...
            if ($(this).parent().attr("class") == "new_group") {
                return;
            }
            _tmpCurGrpNm = $(this).find("span").html();
            $("#contentTitle h1").html(_tmpCurGrpNm);
            closeIfrContent2();
            leftMenuAllOff();
            $(this).parent().addClass("on");
            _searchList.setInqyDsnc("3");
            _searchList.setPageNo(1);
            _searchList.setGrpCd($(this).attr("val-grpcd"));
            _searchList.setSrchWord("");
            parent.setSearchTextDefault();
            //test chen
            var _chkV_type = $("#VIEW_TYPE_CD").val();
            var Df_SetupV = $("#Df_SetupV").val();
            if (Df_SetupV == "true") {
                //modify 20160722 chen 
                if (_chkV_type == "1") {
                    $(".btnbx_r a.btn_thumb").click(); //chen 20160728
                }
                else if (_chkV_type == "2") {
                    $(".btnbx_r a.btn_list").click(); //chen 20160728
                }
                else if (_chkV_type == "3") {
                    $(".btnbx_r a.btn_org").click(); //chen 20160728
                }
                $("#Df_SetupV").val("false");
            }
            _INQ_JOIN_DV = "";
            //by chen 20160729
            if (_chkV_type == "1") {
                _searchList.setPagePerCnt(30);
            }
            else if (_chkV_type == "2") {
                _searchList.setPagePerCnt(10);
                $("#combo_row").text(_searchList.getPagePerCnt());
                $("#pagingPopBtn a:eq(0)").html("<span><em class=\"icon_list\" style=\"display:none;\"></em>" + _searchList.getPagePerCnt() + "</span>");
            }
            else if (_chkV_type == "3") {
            }
            if (_chkV_type == "3") {
                //drawTrcoList();				//직원 목록 표시
                drawBscGrpList(); //기본그룹 목록 표시
                drawUserGrpList(); //사용자그룹 목록 표시	
            }
            else {
                drawTrcoList(); //직원 목록 표시
                drawBscGrpList(); //기본그룹 목록 표시
                drawUserGrpList(); //사용자그룹 목록 표시	
            }
            //loadDefaultConfig();
        });
        //그룹등록 입력창 표시
        this.addEvent("#addGroupBtn", "click", function () {
            //그룹명 변경창이 있으면 닫기
            $("#userGrpList dd .new_group").each(function () {
                $(this).parent().remove();
            });
            $(".new_group #GRP_NM").val("");
            $(".new_group").slideDown(200);
            $(".new_group #GRP_NM").focus();
        });
        //리스드 스테일  20160714 수정
        this.addEvent(".btnbx_r a", "click", function () {
            $(".btnbx_r a").each(function (i) {
                $(this).removeClass("on");
            });
            if ($(this).hasClass("btn_thumb")) {
                if (_searchList.getPagePerCnt() >= "10") {
                    $("#thumb_viewwrap").hide();
                    _searchList.setPagePerCnt(30);
                    thumb_per_page = 30;
                }
                $("#paging_first").click();
                _searchList.setPageNo(1);
                //$.cookie("COOKIE_PAGE_PER_CNT_TRCO", null);
                $(".btnbx_r a.btn_thumb").addClass("on");
                Clk_Vtp = "1";
                $("#VIEW_TYPE_CD").val(Clk_Vtp); //20160714
                drawTrcoList();
                //modify dat: 20160720
            }
            else if ($(this).hasClass("btn_list")) {
                _searchList.setPagePerCnt(10);
                $("#paging_first").click();
                _searchList.setPageNo(1);
                $("#pagingPopBtn a:eq(0)").html("<span><em class=\"icon_list\" style=\"display:none;\"></em>" + _searchList.getPagePerCnt() + "</span>");
                Clk_Vtp = "2";
                $("#VIEW_TYPE_CD").val(Clk_Vtp); //20160714
                $(".btnbx_r a.btn_list").addClass("on");
                drawTrcoList();
            }
            else if ($(this).hasClass("btn_org")) {
                tree_dvsn_cd = $(this).parent().parent().parent().parent().parent().siblings().find("#bscGrpList li.SelectedDpt").attr('val-dvsncd'); //$(this).find("#bscGrpList li a").parent().parent().attr('val-dvsncd');
                //alert(tree_dvsn_cd);
                if (tree_dvsn_cd == "") {
                    //disable clicked
                    _searchList.setPagePerCnt(10);
                    $("#paging_first").click();
                    _searchList.setPageNo(1);
                    $("#pagingPopBtn a:eq(0)").html("<span><em class=\"icon_list\" style=\"display:none;\"></em>" + _searchList.getPagePerCnt() + "</span>");
                    Clk_Vtp = "2";
                    $("#VIEW_TYPE_CD").val(Clk_Vtp); //20160714
                    $(".btnbx_r a.btn_list").addClass("on");
                    drawTrcoList();
                }
                else {
                    Clk_Vtp = "3";
                    $("#VIEW_TYPE_CD").val(Clk_Vtp); //20160714
                    var _loadVtyp3 = on_loadVtyp3;
                    if (on_loadVtyp3 >= 0) {
                        on_loadVtyp3 = -1;
                    }
                    else {
                        on_loadVtyp3 = on_loadVtyp3 - _loadVtyp3;
                    }
                    $(".btnbx_r a.btn_org").addClass("on");
                    //27102016 check silverlight not install
                    drawTrcoList();
                }
            }
        });
        //그룹등록 입력창 숨기기
        this.addEvent("#new_group_cancel", "click", function () {
            $(".new_group").slideUp(200);
        });
        $(".new_group #GRP_NM").keypress(function (e) {
            if (e.which == 13)
                $("#new_group_save").click();
        });
        //그룹등록 처리
        this.addEvent("#new_group_save", "click", function () {
            var objGrpNm = $("#GRP_NM");
            if ($(objGrpNm).val() == "") {
                $(objGrpNm).focus();
                //				parent.showPrintInfoMsg("WE0034");//그룹명을 입력하세요.
                parent.showPrintInfoMsg($.i18n.prop("lang_WE0034"));
                return;
            }
            if (chkStrLength($(objGrpNm).val()) > 100) {
                $(objGrpNm).focus();
                //				parent.showPrintInfoMsg("WE0035");//그룹명은 100자 초과등록하실 수 없습니다.
                parent.showPrintInfoMsg($.i18n.prop("lang_WE0035")); //그룹명은 100자 초과등록하실 수 없습니다.
                return;
            }
            $(".new_group").hide();
            var jexAjax = jex.createAjaxUtil("empl_3002_01_c001");
            jexAjax.set("GRP_NM", $(objGrpNm).val());
            jexAjax.set("GRP_ICON", "6_1");
            jexAjax.execute(function (dat) {
                //parent.showPrintInfoMsg("WM0002");
                drawUserGrpList();
            });
        });
        //그룹명 변경입력창 닫기
        $("#userGrpList dd .new_group a:eq(1) ").live("click", function () {
            $("#userGrpList dd").each(function () {
                $(this).show(); //그룹명 변경에 의해 숨김처리된 그룹항목은 표시처리
            });
            $(this).parent().parent().remove();
        });
        $("#userGrpList dd .new_group input").live("keypress", function (e) {
            if (e.which == 13)
                $("#userGrpList dd .new_group a:eq(0)").click();
        });
        //그룹명 변경처리
        $("#userGrpList dd .new_group a:eq(0) ").live("click", function () {
            var objGrpNm = $("#userGrpList dd .new_group input");
            if ($(objGrpNm).val() == "") {
                $(objGrpNm).focus();
                //				parent.showPrintInfoMsg("WE0034");
                parent.showPrintInfoMsg($.i18n.prop("lang_WE0034"));
                return;
            }
            if (chkStrLength($(objGrpNm).val()) > 100) {
                $(objGrpNm).focus();
                //				parent.showPrintInfoMsg("WE0035");
                parent.showPrintInfoMsg($.i18n.prop("lang_WE0035"));
                return;
            }
            var jexAjax = jex.createAjaxUtil("empl_3003_01_u001");
            jexAjax.set("GRP_CD", _tmpCurGrpCd);
            jexAjax.set("GRP_ICON", _tmpCurGrpIcon);
            jexAjax.set("GRP_NM", $(objGrpNm).val());
            jexAjax.execute(function (dat) {
                //parent.parent.showPrintInfoMsg("WM0003");
                if (_searchList.getGrpCd() == _tmpCurGrpCd) {
                    $("#contentTitle h1").html($(objGrpNm).val());
                }
                $(objGrpNm).parent().parent().remove();
                drawUserGrpList();
            });
        });
        // empty
        $("#empty_close").live("click", function () {
            $("#empty").bPopup().close();
        });
        $("#empty_cancel").live("click", function () {
            $("#empty").bPopup().close();
        });
        //그룹비우기 처리
        this.addEvent(".layer_popup_wrap:eq(1) a:eq(1) ", "click", function () {
            var jexAjax = jex.createAjaxUtil("empl_3005_02_d001");
            jexAjax.set("GRP_CD", _tmpCurGrpCd);
            jexAjax.execute(function (dat) {
                $("#empty").bPopup().close();
                if (_searchList.getGrpCd() == _tmpCurGrpCd) {
                    _INQ_JOIN_DV = "";
                    drawTrcoList();
                }
                drawUserGrpList();
            });
        });
        //icon
        $("#icon_close").live("click", function () {
            $("#layer_popup_wrapIcon").bPopup().close();
        });
        $("#cancel_icon").live("click", function () {
            $("#layer_popup_wrapIcon").bPopup().close();
        });
        //그룹아이콘변경 색상선택
        this.addEvent(".layer_popup_wrap .color_select_wrap ul li ", "click", function () {
            var _this = $(this);
            $(".color_select_wrap ul li").each(function () {
                $(this).removeClass("on");
            });
            _this.addClass("on");
        });
        //그룹아이콘변경 아이콘선택
        this.addEvent(".layer_popup_wrap .icon_select_wrap ul li ", "click", function () {
            var _this = $(this);
            $(".icon_select_wrap ul li").each(function () {
                $(this).removeClass("on");
            });
            _this.addClass("on");
        });
        //그룹아이콘변경 처리
        this.addEvent(".layer_popup_wrap:eq(0) a:eq(1) ", "click", function () {
            var grpColorNo = $(".color_select_wrap ul li").index($(".color_select_wrap ul li[class=on]"));
            var grpIconNo = $(".icon_select_wrap ul li").index($(".icon_select_wrap ul li[class=on]"));
            var grpIcon = (grpIconNo + 1) + "_" + (grpColorNo + 1);
            var jexAjax = jex.createAjaxUtil("empl_3003_01_u001");
            jexAjax.set("GRP_CD", _tmpCurGrpCd);
            jexAjax.set("GRP_ICON", grpIcon);
            jexAjax.set("GRP_NM", _tmpCurGrpNm);
            jexAjax.execute(function (dat) {
                drawUserGrpList();
                $("#layer_popup_wrapIcon").bPopup().close();
            });
        });
        this.addEvent('.container_fix_on td, h1', 'mousedown', function () {
            if (window.getSelection) {
                if (window.getSelection().empty) { // Chrome
                    window.getSelection().empty();
                }
                else if (window.getSelection().removeAllRanges) { // Firefox
                    window.getSelection().removeAllRanges();
                }
            }
            else if (document.selection) { // IE?
                document.selection.empty();
            }
        });
        //그룹삭제 레이어창 닫기(X버튼)
        this.addEvent(".layer_popup_wrap:eq(2) a:eq(0) ", "click", function () {
            $(".layer_popup_wrap:eq(2)").bPopup().close();
        });
        //그룹삭제 레이어창 닫기(취소버튼)
        this.addEvent(".layer_popup_wrap:eq(2) a:eq(2) ", "click", function () {
            $(".layer_popup_wrap:eq(2)").bPopup().close();
        });
        //그룹삭제 처리
        this.addEvent(".layer_popup_wrap:eq(2) a:eq(1) ", "click", function () {
            var jexAjax = jex.createAjaxUtil("empl_3005_01_d001");
            jexAjax.set("GRP_CD", _tmpCurGrpCd);
            jexAjax.execute(function (dat) {
                $("#delete").bPopup().close();
                //현재선택된 그룹을 삭제하거나 그룹이 하나였을때 삭제한 경우라면 전체로
                if (_searchList.getGrpCd() == _tmpCurGrpCd || $("#userGrpList dd").length == 1) {
                    $("#contentTitle h1").html("전체");
                    _searchList.setInqyDsnc("1"); //전체검색으로 변경
                    _searchList.setPageNo(1);
                    _INQ_JOIN_DV = "";
                    drawTrcoList();
                }
                drawUserGrpList();
            });
        });
        //===== 앱 연결하기 =====
        //그린메시지
        //		this.addEvent("#app_greenBtn", "click", function() {  
        //			const userList = [];
        //			if($("input[name=trco]:checked").length == 0){
        //				parent.showPrintInfoMsg("","직원을 선택하세요.");
        //				return true;
        //			}
        //			const isPopupCall = true;
        //			//목록 (체크 목록 가져오기)
        //			$("input[name=trco]:checked").each(function(){
        //				const trcoRec = _datTrcoList[$("input[name=trco]").index(this)];
        //				const clphno = null2void(trcoRec.CLPH_NO).split("-").join("");
        //				const usernm = trcoRec.FLNM;
        //				
        //				if(clphno && usernm){
        //					userList.push({"HP_NO":clphno,"USER_NM":usernm});
        //				}else{
        //					parent.showPrintInfoMsg("","휴대폰 번호가 있는 직원만 그린메시지를 사용하실 수 있습니다.");
        //					isPopupCall = false;
        //					return false;
        //				}
        //			});
        //			if(isPopupCall)
        //			appGreenMeassagePop(userList);
        //		}); 
        //콜라보
        //		this.addEvent("#app_colaBtn", "click", function() {  
        //			const userList = [];
        //			if($("input[name=trco]:checked").length == 0){
        //				parent.showPrintInfoMsg("","직원을 선택하세요.");
        //				return true;
        //			}
        //			//목록 (체크 목록 가져오기)
        //			$("input[name=trco]:checked").each(function(){
        //				const trcoRec = _datTrcoList[$("input[name=trco]").index(this)];
        //				userList.push(trcoRec);
        //			});
        //			appColaboPop(userList);
        //		}); 
        // 일정
        //		this.addEvent("#app_schdBtn", "click", function(){
        //			const userList = [];
        //			if( $("input[name=trco]:checked").length == 0 ){
        //				parent.showPrintInfoMsg("", "직원을 선택하세요.");
        //				return true;
        //			}
        //			// 체크된 사용자 목록 가져오기
        //			$("input[name=trco]:checked").each(function(index, value){
        //				const userRec = _datTrcoList[$("input[name=trco]").index(this)];
        //				userList.push(userRec);
        //			});
        //			appSchedulePop(userList);
        //		});
        //---------------------------
        // Content (목록) 
        //---------------------------
        //등록 -- 신규
        this.addEvent('#addEmplBtn2', 'click', function (e) {
            openIfrContent("empl_1008_01.act");
        });
        //일괄등록
        this.addEvent('#excelTrcoBtn', 'click', function (e) {
            location.href = "empl_1006_01.act";
        });
        //휴지통 복원하기
        this.addEvent("#trcoRestoreBtn", "click", function () {
            if ($("input[name=trco]:checked").length == 0) {
                parent.showPrintInfoMsg("WE0036"); //복원할 직원을 선택하세요.
                return true;
            }
            //선택한 직원을 복원 하시겠습니까?
            if (!confirm(jex.getMsg("WM0008"))) {
                return true;
            }
            var sendTrcoList = [];
            //직원 목록 (체크 목록 가져오기)
            $("input[name=trco]:checked").each(function () {
                var trcoRec = _datTrcoList[$("input[name=trco]").index(this)];
                sendTrcoList.push({
                    "EMPL_IDNT_ID": trcoRec.EMPL_IDNT_ID,
                    "USER_ID": trcoRec.USER_ID,
                    "BSNN_NO": trcoRec.BSNN_NO
                });
            });
            var jexAjax = jex.createAjaxUtil("empl_6001_01_u001");
            jexAjax.set("EMPL_LIST", sendTrcoList);
            jexAjax.execute(function (dat) {
                if (sendTrcoList.length > 0) {
                    parent.showPrintInfoMsg("WM0009"); //복원완료
                    _INQ_JOIN_DV = "";
                    drawTrcoList(); //직원 목록 표시
                    drawBscGrpList(); //기본그룹 목록 표시
                    drawUserGrpList(); //사용자그룹 목록 표시		
                }
            });
        });
        //휴지통 완전삭제
        this.addEvent("#trcoDeleteBtn", "click", function () {
            //삭제할 직원을 선택하세요
            if ($("input[name=trco]:checked").length == 0) {
                parent.showPrintInfoMsg("WE0037");
                return true;
            }
            //선택한 직원을 삭제 하시겠습니까? 삭제하시면 해당직원에 대해 모든 데이터가 삭제됩니다.
            if (!confirm(jex.getMsg("WM0011"))) {
                return true;
            }
            var sendTrcoList = [];
            //목록 (체크 목록 가져오기)
            $("input[name=trco]:checked").each(function () {
                var trcoRec = _datTrcoList[$("input[name=trco]").index(this)];
                sendTrcoList.push({
                    "EMPL_IDNT_ID": trcoRec.EMPL_IDNT_ID
                });
            });
            var jexAjax = jex.createAjaxUtil("empl_6001_02_d001");
            jexAjax.set("EMPL_LIST", sendTrcoList);
            jexAjax.execute(function (dat) {
                if (sendTrcoList.length > 0) {
                    parent.showPrintInfoMsg("WM0007"); //삭제되었습니다.
                    _INQ_JOIN_DV = "";
                    drawTrcoList(); //직원 목록 표시
                    drawBscGrpList(); //기본그룹 목록 표시
                    drawUserGrpList(); //사용자그룹 목록 표시		
                }
            });
        });
        //전체 목록으로
        this.addEvent("#trcoListBtn", "click", function () {
            _tmpCurGrpNm = $("#totEmplBtn").find("a").html();
            $("#contentTitle h1").html(_tmpCurGrpNm);
            _searchList.setInqyDsnc("1");
            _searchList.setPageNo(1);
            _searchList.setSrchWord("");
            parent.setSearchTextDefault();
            _INQ_JOIN_DV = "";
            if ($("#tab_list").length > 0) {
                $("#tab_list").show();
            }
            drawTrcoList(); //직원 목록 표시
            drawBscGrpList(); //기본그룹 목록 표시
            drawUserGrpList(); //사용자그룹 목록 표시		
            leftMenuAllOff();
            $(this).removeClass("on").addClass("on");
            $("#div_content").show();
            $("#ifr_content").hide();
        });
        //그룹지정 팝업div
        this.addEvent('#grpPopupBtn', 'click', function (e) {
            $(this).find(".combo_layer").fadeIn();
        });
        this.addEvent('#grpPopupBtn', 'mouseleave', function (e) {
            $(this).find(".combo_layer").fadeOut();
        });
        //그룹지정 적용
        this.addEvent('#grpSetBtn', 'click', function (e) {
            //그룹으로 지정할 직원을 선택하세요.
            if ($("input[name=trco]:checked").length == 0) {
                //				parent.showPrintInfoMsg("WE0038");
                parent.showPrintInfoMsg($.i18n.prop("lang_WE0038"));
                return true;
            }
            var sendGrpTrcoList = [];
            var selGrpList = []; //선택된 그룹 목록
            var delGrpList = []; //선택안된 그룹 목록
            var selEmplList = []; //선택된 직원 목록
            //직원 목록 (체크 목록 가져오기)
            $("input[name=trco]:checked").each(function () {
                var _this = this;
                //const trcoRec = _datTrcoList[$("input[name=trco]").index(_this)];
                var trcoRec = $(this).parent().find('input[name=EMPL_IDNT_ID]').val();
                var updateStatus = "";
                //기본그룹,사용자그룹 목록에서 체크
                $("#group_layer li").each(function () {
                    var updateStatus = "";
                    var grp_cd = "," + $(this).find("a").attr("val-grpcd") + ",";
                    var grp_cd_list = "," + $(_this).parent().find("input[name=GRP_CD]").val() + ",";
                    //기존에 그룹으로 지정되어 있었으면 등록과정 건너뛰기
                    if (grp_cd_list.indexOf(grp_cd) > -1 && $(this).find("img").attr("src").indexOf("btn_checkbox1.gif") > -1) {
                        return true;
                    }
                    //기존에 그룹으로 없었으면 삭제과정 건너뛰기
                    if (grp_cd_list.indexOf(grp_cd) == -1 && $(this).find("img").attr("src").indexOf("btn_checkbox3.gif") > -1) {
                        return true;
                    }
                    //그룹에 직원 등록
                    if ($(this).find("img").attr("src").indexOf("btn_checkbox1.gif") > -1) {
                        updateStatus = "I";
                    }
                    //그룹에서 직원 삭제
                    else if ($(this).find("img").attr("src").indexOf("btn_checkbox3.gif") > -1) {
                        updateStatus = "D";
                    }
                    //기존과 그룹이 동일하다면 건너뛰기
                    else {
                        return true;
                    }
                    sendGrpTrcoList.push({
                        "EMPL_IDNT_ID": "",
                        "GRP_CD": $(this).find("a").attr("val-grpcd"),
                        /*"TRGT_EMPL_IDNT_ID":trcoRec.EMPL_IDNT_ID,*/
                        "TRGT_EMPL_IDNT_ID": trcoRec,
                        "GRP_DSNC": updateStatus
                    });
                });
                $("#group_layer li").each(function () {
                    if (updateStatus == "I")
                        return true;
                    if ($(this).find("img").attr("src").indexOf("btn_checkbox1.gif") > -1) {
                        updateStatus = "I";
                    }
                    else {
                        updateStatus = "";
                    }
                });
                selEmplList.push({ "TRGT_EMPL_IDNT_ID": trcoRec, "GRP_DV": updateStatus });
            });
            $("#group_layer li").each(function () {
                //선택 안된 상태의 그룹
                if ($(this).find("img").attr("src").indexOf("btn_checkbox3.gif") > -1) {
                    delGrpList.push({ "GRP_CD": $(this).find("a").attr("val-grpcd") });
                }
                else if ($(this).find("img").attr("src").indexOf("btn_checkbox1.gif") > -1) {
                    //선택된 상태의 그룹
                    selGrpList.push({ "GRP_CD": $(this).find("a").attr("val-grpcd") });
                }
            });
            //const jexAjax = jex.createAjaxUtil("empl_3006_01_u001");
            var jexAjax = jex.createAjaxUtil("empl_3006_01_u003");
            jexAjax.set("REC1", selEmplList); //선택된 직원 목록
            jexAjax.set("REC2", delGrpList); //선택안된 그룹 목록
            jexAjax.set("REC3", selGrpList); //선택된 그룹 목록
            //jexAjax.set("GRP_LIST", sendGrpTrcoList);
            jexAjax.execute(function (dat) {
                parent.showPrintInfoMsg($.i18n.prop("lang_WM0010")); //적용되었습니다.
                $("#grpPopupBtn").find(".combo_layer").fadeOut();
                //if(sendGrpTrcoList.length > 0){
                if (selEmplList.length > 0) {
                    _INQ_JOIN_DV = "";
                    drawTrcoList(); //직원 목록 표시
                    drawBscGrpList(); //기본그룹 목록 표시
                    drawUserGrpList(); //사용자그룹 목록 표시		
                }
            });
        });
        //페이징 수 선택 팝업div
        this.addEvent('#pagingPopBtn a:eq(0)', 'click', function (e) {
            $(this).parent().find(".combo_layer").show();
            $("#div_content").css("margin-bottom", "20px");
            parent.ifrMainResize("N", 30); // 20160721 borey
        });
        this.addEvent('#pagingPopBtn', 'mouseleave', function (e) {
            $(this).find(".combo_layer").hide();
            //$("#div_content").css("margin-bottom","");
        });
        //afdfdsfdsafdsaf
        this.addEvent("#pagingPopBtn ul li", "click", function () {
            $.cookie('COOKIE_PAGE_PER_CNT_TRCO', $(this).find("input").val(), { expires: 9999 }); /*$(this).find("label").html()*/
            $("#pagingPopBtn a:eq(0)").html("<span><em class=\"icon_list\" style=\"display:none;\"></em> " + $(this).find("input").val() + "</span>");
            $("#pagingPopBtn").find(".combo_layer").fadeOut();
            _searchList.setPagePerCnt($(this).find("input").val());
            _searchList.setPageNo(1);
            if (excludeEmpl == true) {
                drawTrcoList(0, 0);
            }
            else {
                drawTrcoList(0, 1);
            }
            $("#combo_row").text($(this).find("input").val());
        });
        $('#table_paging').on('click', '.pag_num a', function () {
            var PageNumb = $(this).text();
            _searchList.setPageNo(PageNumb);
            //daravuth 07.27.2016
            if (excludeEmpl == true) {
                drawTrcoList(PageNumb, 0);
            }
            else {
                drawTrcoList(PageNumb);
            }
            //drawTrcoList(PageNumb,1);
        });
        //♥ 안내문구 레이어 
        $("#tbTbody_TRCO_LIST img").live("mouseenter", function (event) {
            if ($(this).attr("src").indexOf("/img/ico/icon_disuse.png") > -1 && $("#str_cnts_msgr_yn").attr("value") == "Y") {
                $("#tip_layerpopup2").css({ "top": $(this).offset().top });
                $("#tip_layerpopup2").show();
            }
            else if ($(this).attr("src").indexOf("/img/ico/icon_disuse.png") > -1 || $(this).attr("src").indexOf("/img/ico/icon_use.png") > -1) {
                $("#tip_layerpopup").css({ "top": $(this).offset().top });
                $("#tip_layerpopup").show();
            }
        });
        $("#tbTbody_TRCO_LIST img").live("mouseleave", function (event) {
            if ($(this).attr("src").indexOf("/img/ico/icon_disuse.png") > -1 && $("#str_cnts_msgr_yn").attr("value") == "Y") {
                $("#tip_layerpopup2").hide();
            }
            else if ($(this).attr("src").indexOf("/img/ico/icon_disuse.png") > -1 || $(this).attr("src").indexOf("/img/ico/icon_use.png") > -1) {
                $("#tip_layerpopup").hide();
            }
        });
        // 30-05-2016
        this.addEvent(".btnSearch", "click", function () {
            parent.searchWord(excludeEmpl);
        });
        //배너 닫기
        this.addEvent("#btn_bnr_close", "click", function () {
            parent.setBannerClose();
            $("#rcmd_bnr").hide();
        });
        //가입 직원
        this.addEvent("#list_join", "click", function () {
            _INQ_JOIN_DV = "Y";
            resetTab();
            $(this).css("font-weight", "bold");
            _searchList.setPageNo(1);
            drawTrcoList(); //직원 목록 표시
        });
        //미가입 직원
        this.addEvent("#list_unjoin", "click", function () {
            _INQ_JOIN_DV = "N";
            resetTab();
            $(this).css("font-weight", "bold");
            _searchList.setPageNo(1);
            drawTrcoList(); //직원 목록 표시
        });
        //전체
        this.addEvent("#list_all", "click", function () {
            _INQ_JOIN_DV = "";
            resetTab();
            $(this).css("font-weight", "bold");
            _searchList.setPageNo(1);
            drawTrcoList(); //직원 목록 표시
        });
        //직원초대 안내 tooltip 다시 보지 않기
        this.addEvent("#rcmd_tip_close", "click", function () {
            $.cookie("COOKIE_RCMD_TOOLTIP", "close", { expires: 9999 });
            $("#rcmd_tip").hide();
        });
        //안내 레이어에서 부서 이동
        this.addEvent("#top_banner_area_dvsn", "click", function () {
            parent.moveDvsn();
        });
        //안내 레이어에서 직위 직책 이동
        this.addEvent("#top_banner_area_use_cd", "click", function () {
            parent.moveUseCd();
        });
        //안내 레이어에서 오늘은 그만보기 버튼 클릭
        this.addEvent("#top_banner_area_not_view_dvsn", "click", function () {
            var expiredays = 1;
            var todayDate = new Date();
            //alert( todayDate.getDate() );
            todayDate.setDate(todayDate.getDate() + expiredays);
            todayDate.setHours(0, 0, 0, 0);
            document.cookie = "top_banner_area_dvsn" + "=" + "view" + "; path=/; expires=" + todayDate.toGMTString() + ";";
            $("#top_banner_area_dvsn").parent().hide();
            dsvn_use_cd_view();
        });
        //안내 레이어에서 오늘은 그만보기 버튼 클릭
        this.addEvent("#top_banner_area_not_view_use_cd", "click", function () {
            var expiredays = 1;
            var todayDate = new Date();
            todayDate.setDate(todayDate.getDate() + expiredays);
            todayDate.setHours(0, 0, 0, 0);
            document.cookie = "top_banner_area_use_cd" + "=" + "view" + "; path=/; expires=" + todayDate.toGMTString() + ";";
            $("#top_banner_area_use_cd").parent().hide();
            dsvn_use_cd_view();
        });
        //더보기 event
        this.addEvent("#th_btn_moreview", "click", function () {
            for (var i = 0; i < 30; i++) {
                thumb_per_page++;
            }
            _searchList.setPagePerCnt(thumb_per_page);
            drawTrcoList();
        });
        //전체 event
        this.addEvent("#list_cnt_all", "click", function () {
            $("#tbTbody_TRCO_LIST").empty();
            addTabBold(this);
            /*_searchList.setPagePerCnt(thumb_per_page);
             _searchList.setPageNo(1);
            _searchList.setPagePerCnt(10);
            
            drawTrcoList();*/
            _searchList.setInqyDsnc("4");
            searchBasket();
        });
        //전체 event
        this.addEvent("#list_trash", "click", function () {
            $("#tbTbody_TRCO_LIST").empty();
            $(".btnbx_r").hide();
            addTabBold(this);
            _searchList.setPagePerCnt(thumb_per_page);
            _searchList.setInqyDsnc("8");
            searchBasket();
        });
        //전체 event
        this.addEvent("#list_stop_termination", "click", function () {
            $("#tbTbody_TRCO_LIST").empty();
            $(".btnbx_r").hide();
            addTabBold(this);
            _searchList.setPagePerCnt(thumb_per_page);
            _searchList.setInqyDsnc("9");
            searchBasket();
        });
        //엑셀 다운로드  event
        this.addEvent("#excel_download", "click", function () {
            excel_download();
        });
    }
}))();
//부서, 직책, 직급 안내 레이어 보여 주기
function dsvn_use_cd_view() {
    //return;
    if ($("#str_cnts_msgr_yn").attr("value") == "Y") {
        //부서 안내 레이어 view
        if ($.cookie("top_banner_area_dvsn")) {
            $("#top_banner_area_dvsn").parent().hide();
        }
        else {
            if (_arrBsunList && _arrBsunList.length > 0) {
                //부서가 있다면
                $("#top_banner_area_dvsn").parent().hide();
            }
            else { //부서가 없다면
                // #5370
                /*$("#top_banner_area_dvsn").parent().show();
                $("#top_banner_area_use_cd").parent().hide();
                parent.ifrMainResize("N");*/
                // /#5370
            }
        }
        //직급,직책 안내 레이어 view
        //		if($.cookie("top_banner_area_use_cd"))
        //		{
        //			$("#top_banner_area_use_cd").parent().hide();
        //		}
        //		else if( $("#top_banner_area_dvsn").parent().css("display")=="none" )
        //		{
        //			
        //			//alert( $("#top_banner_area_dvsn").parent().css("display") );
        //			
        //			//직급,직책 있나 조회
        //			const REC_USER_GRP_DSNC=[{'USER_GRP_DSNC':'UC01010'},{'USER_GRP_DSNC':'UC01020'}];
        //			const jexAjax=jex.createAjaxUtil("empl_0003_01_l001");
        //			jexAjax.set("REC_USER_GRP_DSNC",REC_USER_GRP_DSNC);
        //			jexAjax.execute(function(dat){
        //				
        //				if(dat.COMMON_HEAD.ERROR){
        //					parent.showPrintInfoMsg("",dat.COMMON_HEAD.MESSAGE);
        //					return;
        //				}
        //				/*
        //				const UC01010 = false;
        //				const UC01020 = false;
        //				
        //				$.each(dat.REC_DATA,function(i,val){
        //					if(val.USER_GRP_DSNC == "UC01010" )
        //					{
        //						UC01010 = true;
        //					}
        //					
        //					if(val.USER_GRP_DSNC == "UC01020" )
        //					{
        //						UC01020 = true;
        //					}
        //				});
        //				*/
        //				
        //				if(dat.REC_DATA && dat.REC_DATA.length>0)
        //				{
        //					$("#top_banner_area_use_cd").parent().hide();
        //				}
        //				else
        //				{
        //					$("#top_banner_area_use_cd").parent().show();
        //				}
        //				parent.ifrMainResize("N");
        //			});
        //		}
    }
}
//탭 초기화(가입, 미가입, 전체)
function resetTab() {
    if ($("#tab_list").length > 0) {
        $("#list_join").css("font-weight", "");
        $("#list_unjoin").css("font-weight", "");
        $("#list_all").css("font-weight", "");
    }
}
var _curTrcoNo = "";
//상세화면에서 즐겨찾기 수정할 경우 목록의 즐겨찾기 아이콘도 수정처리 (상세화면에서 호출함)
/*function drawBkmk(bkmkYn){
    const objImg;
    $("#tb_TRCO_LIST tbody input[name=EMPL_IDNT_ID]").each(function(){
        if($(this).val() == _curTrcoNo){
            objImg = $(this).parent().parent().find("a img");
            return false;
        }
    });
    
    if(bkmkYn == "Y"){
        $(objImg).attr("src","../img/ico/icon_bookmark_on.png");
        $("#userBkmkCnt").html(parseInt($("#userBkmkCnt").html())+1);
    }else if(bkmkYn == "N"){
        $(objImg).attr("src","../img/ico/icon_bookmark_off.png");
        $("#userBkmkCnt").html(parseInt($("#userBkmkCnt").html())-1);
    }
}*/
function hideEmpldiv1() {
    $("#div_content").hide();
}
//조회구분 전체로 초기화
function setInqDvDef() {
    _INQ_JOIN_DV = "";
}
// 목록 표시
function drawTrcoList(pageno, viewType) {
    // Detecting IE
    // Get IE or Edge browser version
    var version = detectIE();
    _loding.start();
    if (pageno) {
        _searchList.setPageNo(pageno);
    }
    if (_INQ_JOIN_DV == "") { //전체
        resetTab();
        if ($("#tab_list").length > 0) {
            $("#list_all").css("font-weight", "bold");
        }
    }
    //휴지통인경우 상단 버튼 변경
    if (_searchList.getInqyDsnc() == "4") {
        $("#addEmplBtn2").hide(); //등록
        $("#grpPopupBtn").css("display", "none");
        $("#trcoRestoreBtn").show(); //복원하기
        $("#trcoDeleteBtn").show(); //완전삭제
        $("#trcoListBtn").show(); //목록
        $("#contentTitle h1").html("제외직원 보기");
        $(".btnbx_r").hide();
        $(".schbx_r").show();
        $("#popEmplBtn").hide(); //직원초대
        $("#rcmd_bnr").hide(); //배너
    }
    else if (_searchList.getInqyDsnc() == "8" || _searchList.getInqyDsnc() == "9") {
        $(".btnbx_r").hide();
    }
    else {
        $(".btnbx_r").show();
        if ($("#str_cnts_msgr_yn").attr("value") == "N") {
            $("#addEmplBtn2").hide();
            $("#popEmplBtn").remove();
        }
        else {
            $("#addEmplBtn2").show(); //직원등록
            $("#popEmplBtn").show(); //직원초대
            if (parent.getBannerOpen()) { //배너 Open 
            }
        }
        $("grpPopupBtn").css("display", "block");
        $("#trcoRestoreBtn").hide(); //복원하기
        $("#trcoDeleteBtn").hide(); //완전삭제
        $("#trcoListBtn").hide(); //목록
    }
    var jexAjax = jex.createAjaxUtil("empl_1001_01_l001");
    jexAjax.set("PAGE_PER_CNT", _searchList.getPagePerCnt());
    jexAjax.set("PAGE_NO", _searchList.getPageNo());
    jexAjax.set("INQY_DSNC", _searchList.getInqyDsnc());
    jexAjax.set("GRP_CD", _searchList.getGrpCd());
    jexAjax.set("SRCH_WORD", _searchList.getSrchWord());
    jexAjax.set("BSNN_NO", _searchList.getBsnnNo());
    jexAjax.set("JOIN_YN", _INQ_JOIN_DV); //가입 조회조건
    jexAjax.setErrTrx(false);
    jexAjax.execute(function (dat) {
        console.log(dat);
        if (dat.COMMON_HEAD.ERROR) {
            _loding.stop();
            parent.showPrintInfoMsg("", dat.COMMON_HEAD.MESSAGE);
            return;
        }
        //"true" if no user but an admin
        /*if(dat.EMPL_REC.length == 1 && $("#str_cnts_msgr_yn").val() == "Y"){
            isAdmin = true;
            _loding.stop();
            return;
        }else{
            isAdmin = false;
        }*/
        _datTrcoList = dat.EMPL_REC;
        //건수 표시(전체일때만)
        if (_INQ_JOIN_DV == "" && $("#tab_list").length > 0) {
            if (_searchList.getInqyDsnc() == "1") {
                $("#list_cnt_all").html("전체(" + dat.TOTL_RSLT_CNT + ")");
            }
            else {
                $("#list_cnt_all").html("전체(" + dat.DEL_TRASH_TOTL_RSLT_CNT + ")");
            }
            ;
            $("#list_all_cnt").html("(" + dat.TOTL_RSLT_CNT + ")");
            $("#list_join_cnt").html("(" + dat.CNT_JOIN + ")");
            $("#list_unjoin_cnt").html("(" + dat.CNT_UNJOIN + ")");
            $("#list_trash").html("휴지통(" + dat.TRASH_TOTL_RSLT_CNT + ")");
            $("#list_stop_termination").html("중지/해지(" + dat.DEL_TOTL_RSLT_CNT + ")");
        }
        $("#KOR_DEF_CD").val(dat.CLPH_NTNL_CD); //기본 국가코드
        // 테이블 칼럼의 min-width값을 적용하기 위해 테이블 min-width값 설정
        $("#contentTitle").attr("style", "min-width:" + (345 + (dat.VIEW_REC.length - 2) * 90) + "px;");
        $("#tb_TRCO_LIST").attr("style", "min-width:" + (345 + (dat.VIEW_REC.length - 2) * 90) + "px;");
        //직원상세조회설정 내용에 따라 목록컬럼 순서 변경
        var html_trcoList_colgroup = "<col width=\"30px;\" />";
        html_trcoList_colgroup += "<col width=\"60px;\" />";
        var html_trcoList_thead = "<th scope=\"col\" class=\"fir\" style=\"text-align:center;\"><input type=\"checkbox\" class=\"checkbox\" id=\"allCheck\"/></th>";
        html_trcoList_thead += "<th scope=\"col\" class=\"fir\"></th>";
        var colclphNm = "";
        var colEmlNm = "";
        if (_emplVisibleDat.CLPH_NO_OPPB_YN == 'N') {
            colclphNm = "CLPH_NO";
        }
        if (_emplVisibleDat.EML_OPPB_YN == 'N') {
            colEmlNm = "EML";
        }
        dat.VIEW_REC = fn_rmVblEmlAndClphArr(dat.VIEW_REC, colclphNm, colEmlNm);
        for (var i = 0; i < dat.VIEW_REC.length; i++) {
            if (dat.VIEW_REC[i].KORN_CLMN_NM.indexOf("성별") > -1 || dat.VIEW_REC[i].KORN_CLMN_NM.indexOf("내선번호") > -1) //성별 -> 표시X
                continue;
            if (dat.VIEW_REC[i].KORN_CLMN_NM.indexOf("성명") > -1) { //성명 
                html_trcoList_colgroup += "<col width=\"12%\" />";
                html_trcoList_thead += "<th scope=\"col\" i18nCd='lang_" + dat.VIEW_REC[i].CLMN_NM + "'>" + $.i18n.prop("lang_" + dat.VIEW_REC[i].CLMN_NM) + "</th>";
            }
            else if (dat.VIEW_REC[i].KORN_CLMN_NM.indexOf("휴대폰번호") > -1) { //휴대폰번호            	
                html_trcoList_colgroup += "<col width=\"18%\"  />";
                html_trcoList_thead += "<th scope=\"col\" i18nCd='lang_" + dat.VIEW_REC[i].CLMN_NM + "'>" + $.i18n.prop("lang_" + dat.VIEW_REC[i].CLMN_NM) + "</th>";
            }
            else if (dat.VIEW_REC[i].KORN_CLMN_NM.indexOf("회사전화번호") > -1) { //회사전화번호{
                html_trcoList_colgroup += "<col width=\"18%\"  />";
                html_trcoList_thead += "<th scope=\"col\" i18nCd='lang_" + dat.VIEW_REC[i].CLMN_NM + "'>" + $.i18n.prop("lang_" + dat.VIEW_REC[i].CLMN_NM) + "</th>";
            }
            else { //회사전화번호
                html_trcoList_colgroup += "<col width=\"12%\"  />";
                html_trcoList_thead += "<th scope=\"col\" i18nCd='lang_" + dat.VIEW_REC[i].CLMN_NM + "'>" + $.i18n.prop("lang_" + dat.VIEW_REC[i].CLMN_NM) + "</th>"; //dat.VIEW_REC[i].KORN_CLMN_NM
            }
        }
        $("#tbColgroup_TRCO_LIST").html(html_trcoList_colgroup);
        $("#tbThead_TRCO_LIST").html(html_trcoList_thead);
        //직원 목록 표시
        var html_trcoList = ""; //List data
        var html_imageList = "";
        var html_imageList_pre_data = "";
        for (var i = 0; i < dat.EMPL_REC.length; i++) {
            html_trcoList += "    <tr class=\"" + (i == 0 ? "fir" : "") + "\">";
            html_trcoList += "        <td style=\"text-align:center;\">";
            html_trcoList += "        	<input type=\"checkbox\"	class=\"checkbox\" 	name=\"trco\"/>";
            html_trcoList += "        	<input type=\"hidden\"  						name=\"PRFL_PHTG\" 		value=\"" + dat.EMPL_REC[i].PRFL_PHTG + "\" />";
            html_trcoList += "        	<input type=\"hidden\" 							name=\"GRP_CD\" 		value=\"" + dat.EMPL_REC[i].GRP_CD + "\" />";
            html_trcoList += "        	<input type=\"hidden\" 							name=\"EMPL_IDNT_ID\" 	value=\"" + dat.EMPL_REC[i].EMPL_IDNT_ID + "\" />";
            html_trcoList += "        	<input type=\"hidden\" 							name=\"EML\" 			value=\"" + dat.EMPL_REC[i].EML + "\" />";
            html_trcoList += "        	<input type=\"hidden\" 							name=\"FLNM\" 			value=\"" + dat.EMPL_REC[i].FLNM + "\" />";
            html_trcoList += "        	<input type=\"hidden\" 							name=\"CLPH_NO\" 		value=\"" + dat.EMPL_REC[i].CLPH_NO + "\" />";
            html_trcoList += "        	<input type=\"hidden\" 							name=\"CLPH_NTNL_CD\" 	value=\"" + dat.EMPL_REC[i].CLPH_NTNL_CD + "\" />";
            html_trcoList += "        	<input type=\"hidden\" 							name=\"BSNN_NM\" 		value=\"" + dat.EMPL_REC[i].BSNN_NM + "\" />";
            html_trcoList += "        </td>";
            html_trcoList += "        <td style=\"cursor:pointer;\">";
            //update v2 20160325
            var imgPRFL_PHTG = null2void(dat.EMPL_REC[i].PRFL_PHTG);
            var UsrId = null2void(dat.EMPL_REC[i].USER_ID);
            html_trcoList += "<div class=\"photo\">";
            html_trcoList += "<div class=\"photo_po\">";
            html_trcoList += "<span class=\"r_bg\"></span>";
            if (imgPRFL_PHTG != "") {
                html_trcoList += "<img src=\"" + imgPRFL_PHTG + "\" alt=\"\">";
            }
            else {
                html_trcoList += "<img src=\"../img/img_nullphoto.gif\" alt=\"\">";
            }
            html_trcoList += "        </div></div></div>";
            html_trcoList += "        </td>";
            for (var j = 0; j < dat.VIEW_REC.length; j++) {
                //자택 주소
                if (dat.VIEW_REC[j].CLMN_NM == "OOH_POST_ADRS") {
                    var _ooh_adrs = null2void(dat.EMPL_REC[i].OOH_POST_ADRS, "") + " " + null2void(dat.EMPL_REC[i].OOH_DTL_ADRS, "");
                    html_trcoList += "        <td style=\"text-align:left;cursor:pointer;\"><span class=\"ellipsis\">" + _ooh_adrs + "</span></td>";
                }
                //근무지 주소
                else if (dat.VIEW_REC[j].CLMN_NM == "DTPL_POST_ADRS") {
                    var _dtpl_adrs = null2void(dat.EMPL_REC[i].DTPL_POST_ADRS, "") + " " + null2void(dat.EMPL_REC[i].DTPL_DTL_ADRS, "");
                    html_trcoList += "        <td style=\"text-align:left;cursor:pointer;\"><span class=\"ellipsis\">" + _dtpl_adrs + "</span></td>";
                }
                //성별 -> 표시X
                else if (dat.VIEW_REC[j].CLMN_NM == "GNDR") {
                }
                //음력양력
                else if (dat.VIEW_REC[j].CLMN_NM == "LNR_SLCN") {
                    var _lnr_slcn = (null2void(dat.EMPL_REC[i].LNR_SLCN, "") == "1" ? "음력" : (null2void(dat.EMPL_REC[i].LNR_SLCN, "") == "2" ? "양력" : ""));
                    html_trcoList += "        <td style=\"cursor:pointer;\"><span class=\"ellipsis\">" + _lnr_slcn + "</span></td>";
                }
                //결혼여부
                else if (dat.VIEW_REC[j].CLMN_NM == "WDNG_YN") {
                    var _wdng_yn = (null2void(dat.EMPL_REC[i].WDNG_YN, "") == "Y" ? "기혼" : (null2void(dat.EMPL_REC[i].WDNG_YN, "") == "N" ? "미혼" : ""));
                    html_trcoList += "        <td style=\"cursor:pointer;\"><span class=\"ellipsis\">" + _wdng_yn + "</span></td>";
                }
                else {
                    if ((dat.VIEW_REC[j].CLMN_NM == "EXNM_NO" && null2void(eval("dat.EMPL_REC[" + i + "].EXNM_NO_NTNL_CD"), "") == $("#KOR_DEF_CD").val()) || (dat.VIEW_REC[j].CLMN_NM == "OOTL_NO" && null2void(eval("dat.EMPL_REC[" + i + "].OOTL_NO_NTNL_CD"), "") == $("#KOR_DEF_CD").val())) { //내선번호 포맷 지정
                        //						const _tel_no 	= getWorldNo(null2void(dat.EMPL_REC[i].CMPN_TLPH_NO));
                        //						const _exnm_no 	= null2void(dat.EMPL_REC[i].EXNM_NO);
                        var _tel_no = getWorldNo(null2void(dat.EMPL_REC[i].EXNM_NO));
                        if ($("#NTCD_RPSN_YN").val() == "N")
                            _tel_no = getTel(null2void(dat.EMPL_REC[i].EXNM_NO));
                        var _exnm_no = null2void(dat.EMPL_REC[i].CMPN_TLPH_NO);
                        if (_exnm_no != null && _exnm_no.length > 0) {
                            _exnm_no = " (" + _exnm_no + ")";
                            _exnm_no = _tel_no + _exnm_no;
                        }
                        else {
                            _exnm_no = _tel_no;
                        }
                        ;
                        html_trcoList += "        <td style=\"cursor:pointer;\" ><span class=\"ellipsis\">" + _exnm_no + "</span></td>";
                    }
                    else if ((dat.VIEW_REC[j].CLMN_NM == "CLPH_NO")) { //휴대폰번호 
                        var _clph_no = getWorldNo(null2void(eval("dat.EMPL_REC[" + i + "]." + dat.VIEW_REC[j].CLMN_NM), ""), null2void(eval("dat.EMPL_REC[" + i + "].CLPH_NTNL_CD"), ""));
                        if ($("#NTCD_RPSN_YN").val() == "N")
                            _clph_no = getTel(null2void(dat.EMPL_REC[i].CLPH_NO));
                        var _title_clph_no = _clph_no;
                        if (_clph_no.length > 17)
                            _clph_no = _clph_no.substring(0, 17) + "···";
                        html_trcoList += "        <td style=\"cursor:pointer;\" ><span class=\"ellipsis\">" + _clph_no + "</span></td>";
                    }
                    else if (dat.VIEW_REC[j].CLMN_NM == "OOTL_NO") {
                        var _ootl_no = getWorldNo(null2void(eval("dat.EMPL_REC[" + i + "]." + dat.VIEW_REC[j].CLMN_NM), ""), null2void(eval("dat.EMPL_REC[" + i + "].OOTL_NO_NTNL_CD"), ""));
                        if ($("#NTCD_RPSN_YN").val() == "N")
                            _ootl_no = getTel(null2void(dat.EMPL_REC[i].OOTL_NO).replace(/-/gi, "")).replace(/\-/g, " ");
                        html_trcoList += "        <td style=\"cursor:pointer;\"><span class=\"ellipsis\">" + _ootl_no + "</span></td>";
                    }
                    else if (dat.VIEW_REC[j].CLMN_NM == "EXNM_NO") {
                        //						const _cmpn_tlph_no = getWorldNo(null2void(dat.EMPL_REC[i].CMPN_TLPH_NO), null2void(eval("dat.EMPL_REC["+i+"].EXNM_NO_NTNL_CD"),""));
                        //						const _exnm_no = null2void(dat.EMPL_REC[i].EXNM_NO);
                        var _cmpn_tlph_no = getWorldNo(null2void(dat.EMPL_REC[i].EXNM_NO), null2void(eval("dat.EMPL_REC[" + i + "].EXNM_NO_NTNL_CD"), ""));
                        if ($("#NTCD_RPSN_YN").val() == "N")
                            _cmpn_tlph_no = getTel(null2void(dat.EMPL_REC[i].EXNM_NO));
                        var _exnm_no = null2void(dat.EMPL_REC[i].CMPN_TLPH_NO);
                        if (_exnm_no != null && _exnm_no.length > 0) {
                            _exnm_no = " (" + _exnm_no + ")";
                            _exnm_no = _cmpn_tlph_no + _exnm_no;
                        }
                        else {
                            _exnm_no = _cmpn_tlph_no;
                        }
                        ;
                        html_trcoList += "        <td style=\"cursor:pointer;\" ><span class=\"ellipsis\">" + _exnm_no + "</span></td>";
                    }
                    else if (dat.VIEW_REC[j].CLMN_NM == "FLNM") {
                        var _flname = null2void(dat.EMPL_REC[i].FLNM, "");
                        if ($("#str_cnts_msgr_yn").attr("value") == "Y") {
                            if (UsrId != "") {
                                html_trcoList += "        <td style=\"cursor:pointer;\" >" + _flname + " " + "<img src=\"../img/ico/icon_bizcheck_on.png\" alt=\"비즈플레이 가입\" class=\"bizuse\"></td>";
                            }
                            else {
                                html_trcoList += "        <td style=\"cursor:pointer;\" >" + _flname + " " + "<img src=\"../img/ico/icon_bizcheck_off.png\" alt=\"비즈플레이 가입\" class=\"bizuse\"></td>";
                            }
                        }
                        else {
                            html_trcoList += "        <td style=\"cursor:pointer;\" >" + _flname + "</td>";
                        }
                    }
                    else {
                        if (dat.VIEW_REC[j].CLMN_NM == "EXNM_NO")
                            continue;
                        html_trcoList += "        <td style=\"cursor:pointer;\"><span class=\"ellipsis\">" + null2void(eval("dat.EMPL_REC[" + i + "]." + dat.VIEW_REC[j].CLMN_NM), "") + "</span></td>";
                    }
                }
            }
            //컨텐츠사용여부
            html_trcoList += "    </tr>";
            //<!-- 썸네일 보기 -->
            html_imageList += "<li><div class=\"thumb_bx\" style=\"cursor:pointer;\">"; //List box data
            html_imageList += "<div class=\"thumb_chk\">";
            html_imageList += "<input type=\"checkbox\" class=\"checkbox\" name=\"trco\">";
            html_imageList += "        	<input type=\"hidden\"  	name=\"PRFL_PHTG\" 		value=\"" + dat.EMPL_REC[i].PRFL_PHTG + "\" />";
            html_imageList += "        	<input type=\"hidden\" 		name=\"GRP_CD\" 		value=\"" + dat.EMPL_REC[i].GRP_CD + "\" />";
            html_imageList += "        	<input type=\"hidden\" 		name=\"EMPL_IDNT_ID\" 	value=\"" + dat.EMPL_REC[i].EMPL_IDNT_ID + "\" />";
            html_imageList += "        	<input type=\"hidden\" 		name=\"EML\" 			value=\"" + dat.EMPL_REC[i].EML + "\" />";
            html_imageList += "        	<input type=\"hidden\" 		name=\"FLNM\" 			value=\"" + dat.EMPL_REC[i].FLNM + "\" />";
            html_imageList += "        	<input type=\"hidden\" 		name=\"CLPH_NO\" 		value=\"" + dat.EMPL_REC[i].CLPH_NO + "\" />";
            html_imageList += "        	<input type=\"hidden\" 		name=\"CLPH_NTNL_CD\" 	value=\"" + dat.EMPL_REC[i].CLPH_NTNL_CD + "\" />";
            html_imageList += "        	<input type=\"hidden\" 		name=\"BSNN_NM\" 		value=\"" + dat.EMPL_REC[i].BSNN_NM + "\" />";
            html_imageList += "        	<input type=\"hidden\" 		name=\"VIEW_TYPE\" 		value=\"" + dat.VIEW_REC[0].VIEW_TYPE + "\" />";
            html_imageList += "</div>";
            html_imageList += "<div class=\"thumb_top\">";
            if (imgPRFL_PHTG != "") {
                html_imageList += "<img src=\"" + imgPRFL_PHTG + "\" alt=\"\">";
            }
            else {
                html_imageList += "<img src=\"../img/img_nullphoto_b.gif\" alt=\"\">";
            }
            html_imageList += "</div>";
            if (dat.EMPL_REC[i].FLNM.length != "") {
                html_imageList += "<div class=\"thumb_info\"><span class=\"bg\"></span><div class=\"info_txt1\" title=\"" + dat.EMPL_REC[i].FLNM + "\"><span class=\"ellipsis\">" + dat.EMPL_REC[i].FLNM + "<span>";
                //            	html_imageList += "<div class=\"thumb_info\"><span class=\"bg\"></span><div style=\"margin:0px!important\" class=\"info_txt1\" title=\"" + dat.EMPL_REC[i].FLNM + "\">";
                if ($("#str_cnts_msgr_yn").attr("value") == "Y") {
                    if (dat.EMPL_REC[i].USER_ID != "") {
                        html_imageList += "<img src=\"../img/ico/icon_bizcheck_on.png\" alt=\"비즈플레이 가입\">";
                    }
                    else {
                        html_imageList += "<img src=\"../img/ico/icon_bizcheck_off.png\" alt=\"비즈플레이 가입\">";
                    }
                }
                html_imageList += "</div>";
            }
            /*else{
                html_imageList += "<div class=\"thumb_info\"><span class=\"bg\"></span><div class=\"info_txt1\">"+dat.EMPL_REC[i].FLNM;
                        if(dat.EMPL_REC[i].USER_ID != null || dat.EMPL_REC[i].USER_ID != ""){
                            html_imageList +="<img src=\"../img/ico/icon_bizcheck_on.png\" alt=\"비즈플레이 가입\">";
//							console.log(3);
                        }else{
                            
                            html_imageList +="<img src=\"../img/ico/icon_bizcheck_off.png\" alt=\"비즈플레이 가입\">";
//							console.log(4);
                        }
                html_imageList +=	"</div>";
            }*/
            //Laoti 12/12/19
            //email = EML_OPPB_YN
            if (dat.EMPL_REC[i].DVSN_NM == null || dat.EMPL_REC[i].DVSN_NM == "") {
                html_imageList += "<div class=\"info_txt2\"><span class=\"txt_nm\">" + dat.EMPL_REC[i].JBCL_NM + "</span></div>";
            }
            else if (dat.EMPL_REC[i].JBCL_NM == null || dat.EMPL_REC[i].JBCL_NM == "") {
                html_imageList += "<div class=\"info_txt2\"><span class=\"txt_nm\">" + dat.EMPL_REC[i].DVSN_NM + "</span></div>";
            }
            else {
                html_imageList += "<div class=\"info_txt2 ellipsis\"><span class=\"txt_nm\">" + dat.EMPL_REC[i].DVSN_NM + "</span>/" + dat.EMPL_REC[i].JBCL_NM + "</div>";
            }
            if (_emplVisibleDat.CLPH_NO_OPPB_YN == "Y") { //check condition to show or not
                html_imageList += "<div class=\"infobx\">";
                var img_clph_no = getWorldNo(null2void(eval("dat.EMPL_REC[" + i + "].CLPH_NO"), ""), null2void(eval("dat.EMPL_REC[" + i + "].CLPH_NTNL_CD"), ""));
                if ($("#NTCD_RPSN_YN").val() == "N")
                    img_clph_no = getTel(null2void(dat.EMPL_REC[i].CLPH_NO));
                if (img_clph_no.length > 17)
                    img_clph_no = img_clph_no.substring(0, 17) + "···";
                html_imageList += "<p class=\"ellipsis\" style=\"font-size:12px; letter-spacing: 0px;\" title=\"" + img_clph_no + "\"><strong><span class='txt'>M</span></strong>" + img_clph_no + "</p>";
                //company phone number
                //			if(dat.EMPL_REC[i].CMPN_TLPH_NO == null || dat.EMPL_REC[i].CMPN_TLPH_NO == ""){
                if (dat.EMPL_REC[i].EXNM_NO == null || dat.EMPL_REC[i].EXNM_NO == "") {
                    html_imageList += "<p style=\"font-size:12px; letter-spacing: 0px;\"><strong><span class='txt'>T</span></strong >";
                }
                else {
                    var img_exnm_no = getWorldNo(dat.EMPL_REC[i].EXNM_NO, null);
                    if ($("#NTCD_RPSN_YN").val() == "N")
                        img_exnm_no = getTel(null2void(dat.EMPL_REC[i].EXNM_NO));
                    //				html_imageList += "<p class=\"ellipsis\"  style=\"font-size:12px; letter-spacing: 0px;\" title=\""+getWorldNo(dat.EMPL_REC[i].CMPN_TLPH_NO,null)+"("+dat.EMPL_REC[i].EXNM_NO+")\"><strong><span class='txt'>T</span></strong>"+getWorldNo(dat.EMPL_REC[i].CMPN_TLPH_NO,null);
                    html_imageList += "<p class=\"ellipsis\"  style=\"font-size:12px; letter-spacing: 0px;\" title=\"" + img_exnm_no + "(" + dat.EMPL_REC[i].CMPN_TLPH_NO + ")\"><strong><span class='txt'>T</span></strong>" + img_exnm_no;
                }
                //			if(dat.EMPL_REC[i].EXNM_NO == null || dat.EMPL_REC[i].EXNM_NO == ""){
                if (dat.EMPL_REC[i].CMPN_TLPH_NO == null || dat.EMPL_REC[i].CMPN_TLPH_NO == "") {
                }
                else {
                    html_imageList += "(" + dat.EMPL_REC[i].CMPN_TLPH_NO + ")";
                }
                ;
                html_imageList += "</p></div></div></div></li>"; //ends of list
            }
        }
        //<!-- 썸네일 보기 -->
        //REC View type update 20160329
        if (dat.VIEW_REC[0].VIEW_TYPE == "" || dat.VIEW_REC[0].VIEW_TYPE == null) {
            VType = "2"; // 20160721 borey
        }
        else {
            VType = dat.VIEW_REC[0].VIEW_TYPE;
        }
        $("#NTCD_RPSN_YN").val(dat.VIEW_REC[0].NTCD_RPSN_YN);
        parent.$("#str_ntcd_rpsn_yn").val(dat.VIEW_REC[0].NTCD_RPSN_YN);
        thumbTotal = dat.TOTL_RSLT_CNT;
        var srchTxt = $.trim($("#frmSearch").find("input[type=text]").val());
        if (dat.EMPL_REC.length == 0) {
            $("#grpPopupBtn").css("display", "none");
            $(".result_none_wrap").hide();
            $("#srcImg").hide();
            $("#noSrcImg").hide();
            if ($("#frmSearch").find("input[type=text]").is(":focus")) {
                $(".list_table , .paging_wrap").hide();
                if (srchTxt == "") {
                    $("#tab_list li").html("<li><span i18nCd='lang_all'>" + $.i18n.prop("lang_all") + "</span><span class=\"no\" id=\"list_all_cnt\">(" + dat.TOTL_RSLT_CNT + ")</span></li>"); //명
                }
                else {
                    $("#tab_list li").html("<li>'" + srchTxt + "' <span i18nCd='lang_searchEmpl'>" + $.i18n.prop("lang_searchEmpl") + "</span><span class=\"no\" id=\"list_all_cnt\">(" + dat.TOTL_RSLT_CNT + ")</span></li>"); //명
                }
                $(".result_none_wrap").show();
                $("#noSrcImg").hide();
                $("#srcImg").show();
            }
            else if (srchTxt != "") {
                $(".list_table , .paging_wrap").hide();
                $("#tab_list li").html("<li>'" + srchTxt + "' <span i18nCd='lang_searchEmpl'>" + $.i18n.prop("lang_searchEmpl") + "</span><span class=\"no\" id=\"list_all_cnt\">(" + dat.TOTL_RSLT_CNT + ")</span></li>"); //명
                $(".result_none_wrap").show();
                $("#noSrcImg").hide();
                $("#srcImg").show();
            }
            else {
                $("#tab_list li").html("<li><span i18nCd='lang_all'>" + $.i18n.prop("lang_all") + "</span><span class=\"no\" id=\"list_all_cnt\">(" + dat.TOTL_RSLT_CNT + ")</span></li>");
                html_trcoList += " <tr class=\"fir\"><td colspan=\"" + $("#tb_TRCO_LIST colgroup col").length + "\" style=\"text-align:center;\" i18nCd='lang_noList'>" + $.i18n.prop("lang_noList") + "</td></tr>";
            }
        }
        else {
            $("#grpPopupBtn").css("display", "block");
            $(".result_none_wrap").hide();
            if ($("#frmSearch").find("input[type=text]").is(":focus")) {
                $(".list_table , .paging_wrap").hide();
                if (srchTxt == "") {
                    $("#tab_list li").html("<li><span i18nCd='lang_all'>" + $.i18n.prop("lang_all") + "</span><span class=\"no\" id=\"list_all_cnt\">(" + dat.TOTL_RSLT_CNT + ")</span></li>"); //명
                }
                else {
                    $("#tab_list li").html("<li>'" + srchTxt + "' <span i18nCd='lang_searchEmpl'>" + $.i18n.prop("lang_searchEmpl") + "</span><span class=\"no\" id=\"list_all_cnt\">(" + dat.TOTL_RSLT_CNT + "명)</span></li>");
                }
            }
            else if (srchTxt != "") {
                $(".list_table , .paging_wrap").hide();
                $("#tab_list li").html("<li>'" + srchTxt + "' <span i18nCd='lang_searchEmpl'>" + $.i18n.prop("lang_searchEmpl") + "</span><span class=\"no\" id=\"list_all_cnt\">(" + dat.TOTL_RSLT_CNT + ")</span></li>"); //명
            }
            else {
                //change 29/04/2016		
                $("#tab_list li").html("<li><span i18nCd='lang_all'>" + $.i18n.prop("lang_all") + "</span><span class=\"no\" id=\"list_all_cnt\">(" + dat.TOTL_RSLT_CNT + ")</span></li>");
            }
        }
        //daravuth on 07.27.2016
        if (excludeEmpl == true) {
            if (viewType == 0) {
                $("#tbTbody_TRCO_LIST").html(html_trcoList);
                $("#tab_list").show();
                $("#grpPopupBtn").css("display", "none");
                $("#btn_org_tree").hide();
                if (dat.EMPL_REC.length == 0) {
                    if ($("#frmSearch").find("input[type=text]").is(":focus")) {
                        $(".list_table , .paging_wrap").show();
                        $(".combo_box4").hide();
                        $("#myOrgArea").hide();
                        $(".result_none_wrap").hide();
                        $("#tbTbody_TRCO_LIST").html("<tr class=\"fir\"><td colspan=\"" + $("#tb_TRCO_LIST colgroup col").length + "\" style=\"text-align:center;\" i18nCd='lang_noList'>" + $.i18n.prop("lang_noList") + "</td></tr>");
                    }
                    else if (srchTxt != "") {
                        $(".list_table , .paging_wrap").show();
                        $(".combo_box4").hide();
                        $(".result_none_wrap").hide();
                        $("#myOrgArea").hide();
                        $("#tab_list li").html("<li><span i18nCd='lang_all'>" + $.i18n.prop("lang_all") + "</span><span class=\"no\" id=\"list_all_cnt\">(" + dat.TOTL_RSLT_CNT + ")</span></li>");
                        $("#tbTbody_TRCO_LIST").html("<tr class=\"fir\"><td colspan=\"" + $("#tb_TRCO_LIST colgroup col").length + "\" style=\"text-align:center;\" i18nCd='lang_noList'>" + $.i18n.prop("lang_noList") + "</td></tr>");
                    }
                    else {
                        $(".result_none_wrap").hide();
                        $("#myOrgArea").hide();
                        $(".thumb_viewwrap").hide();
                        $("#tab_list li").html("<li><span i18nCd='lang_all'>" + $.i18n.prop("lang_all") + "</span><span class=\"no\" id=\"list_all_cnt\">(" + dat.TOTL_RSLT_CNT + ")</span></li>");
                        $("#tbTbody_TRCO_LIST").html("<tr class=\"fir\"><td colspan=\"" + $("#tb_TRCO_LIST colgroup col").length + "\" style=\"text-align:center;\" i18nCd='lang_noList'>" + $.i18n.prop("lang_noList") + "</td></tr>");
                    }
                }
                else {
                    $(".result_none_wrap").hide();
                    $("#myOrgArea").hide();
                    $(".list_table , .paging_wrap").show();
                    $(".thumb_viewwrap , .btn_moreview").hide();
                    $(".combo_title").show();
                }
                //_searchList.setTotlPageNcnt(dat.TOTL_PAGE_NCNT);
                //drawTablePaing("table_paging", drawTrcoList, dat.PAGE_NO, _searchList.getTotlPageNcnt());
            }
            //excludeEmpl = false;
        }
        else {
            //제외직원 보기 
            //if(viewType == 2){ //20160722 chen
            if (viewType == 1) {
                $("#tbTbody_TRCO_LIST").html(html_trcoList);
                $("#tab_list").show();
                $("#grpPopupBtn").css("display", "block");
                $("#btn_org_tree").hide();
                //				if(dat.EMPL_REC.length == 0){
                //					if($("#frmSearch").find("input[type=text]").is(":focus")){
                //						$(".list_table , .paging_wrap").hide();
                //						$(".combo_box4").hide();
                //						$(".result_none_wrap").show();
                //					}else if(srchTxt != ""){
                //						$(".list_table , .paging_wrap").hide();
                //						$(".combo_box4").hide();
                //						$(".result_none_wrap").show();
                //					}else{
                //						
                //						$(".result_none_wrap").hide();
                //						//change 29/04/2016			
                //						$("#tab_list li").html("<li><span i18nCd='lang_all'>"+$.i18n.prop("lang_all")+"</span><span class=\"no\" id=\"list_all_cnt\">("+dat.TOTL_RSLT_CNT+")</span></li>");
                //						$("#tbTbody_TRCO_LIST").html("<tr class=\"fir\"><td colspan=\""+$("#tb_TRCO_LIST colgroup col").length+"\" style=\"text-align:center;\" i18nCd='lang_noList'>"+$.i18n.prop("lang_noList")+"</td></tr>");
                //					}
                if (dat.EMPL_REC.length == 0) {
                    if ($("#frmSearch").find("input[type=text]").is(":focus")) {
                        $(".list_table , .paging_wrap").show();
                        $(".combo_box4").hide();
                        $("#myOrgArea").hide();
                        $(".result_none_wrap").hide();
                        $("#tbTbody_TRCO_LIST").html("<tr class=\"fir\"><td colspan=\"" + $("#tb_TRCO_LIST colgroup col").length + "\" style=\"text-align:center;\" i18nCd='lang_noList'>" + $.i18n.prop("lang_noList") + "</td></tr>");
                    }
                    else if (srchTxt != "") {
                        $(".list_table , .paging_wrap").show();
                        $(".combo_box4").hide();
                        $(".result_none_wrap").hide();
                        $("#myOrgArea").hide();
                        $("#tab_list li").html("<li><span i18nCd='lang_all'>" + $.i18n.prop("lang_all") + "</span><span class=\"no\" id=\"list_all_cnt\">(" + dat.TOTL_RSLT_CNT + ")</span></li>");
                        $("#tbTbody_TRCO_LIST").html("<tr class=\"fir\"><td colspan=\"" + $("#tb_TRCO_LIST colgroup col").length + "\" style=\"text-align:center;\" i18nCd='lang_noList'>" + $.i18n.prop("lang_noList") + "</td></tr>");
                    }
                    else {
                        $(".result_none_wrap").hide();
                        $("#myOrgArea").hide();
                        $(".thumb_viewwrap").hide();
                        $("#tab_list li").html("<li><span i18nCd='lang_all'>" + $.i18n.prop("lang_all") + "</span><span class=\"no\" id=\"list_all_cnt\">(" + dat.TOTL_RSLT_CNT + ")</span></li>");
                        $("#tbTbody_TRCO_LIST").html("<tr class=\"fir\"><td colspan=\"" + $("#tb_TRCO_LIST colgroup col").length + "\" style=\"text-align:center;\" i18nCd='lang_noList'>" + $.i18n.prop("lang_noList") + "</td></tr>");
                    }
                }
                else {
                    $(".result_none_wrap").hide();
                    $("#myOrgArea").hide();
                    $(".list_table , .paging_wrap").show();
                    $(".thumb_viewwrap , .btn_moreview").hide();
                }
                //					_searchList.setTotlPageNcnt(dat.TOTL_PAGE_NCNT);
                //					drawTablePaing("table_paging", drawTrcoList, dat.PAGE_NO, _searchList.getTotlPageNcnt());
            }
            else { //if(VType)
                //console.log("PageNumb : ", pageno, viewType );
                //나의 그룹 11052016
                if (Clk_Vtp) {
                    // Detecting IE
                    // Get IE or Edge browser version
                    //daravuth comment 27102016
                    //const version = detectIE();
                    if (version === false) {
                        //console.log("IE", version);
                        if (Clk_Vtp == "3") {
                            if (on_loadVtyp3 == -1) {
                                indexObj.init();
                                $("#myOrgArea").children().next().remove();
                                TREE_LOAD == false;
                            }
                            else {
                                if (TREE_LOAD == false) {
                                    indexObj.init();
                                    $("#myOrgArea").children().next().remove();
                                    TREE_LOAD = false;
                                }
                                else {
                                    TREE_LOAD = false;
                                }
                            }
                            orgBusinessObj.selectNode(tree_dvsn_cd); //modify20160713
                        }
                        else {
                            $("#thumb_view_List").html(html_imageList); //append list as box
                            $("#tbTbody_TRCO_LIST").html(html_trcoList); //append list
                        }
                    }
                    else if (version >= 10) {
                        if (Clk_Vtp == "3") {
                            if (on_loadVtyp3 == -1) {
                                indexObj.init();
                                $("#myOrgArea").children().next().remove();
                                TREE_LOAD == false;
                            }
                            else {
                                if (TREE_LOAD == false) {
                                    indexObj.init();
                                    $("#myOrgArea").children().next().remove();
                                    TREE_LOAD = false;
                                }
                                else {
                                    TREE_LOAD = false;
                                }
                            }
                            //							indexObj.init();
                            //							$("#myOrgArea").children().next().remove();
                            orgBusinessObj.selectNode(tree_dvsn_cd); //modify20160713
                        }
                        else {
                            $("#thumb_view_List").html(html_imageList); //append 2nd 
                            $("#tbTbody_TRCO_LIST").html(html_trcoList);
                        }
                    }
                    else if (version <= 9) {
                        if (Clk_Vtp == "3") {
                            //console.log("on_loadVtyp3", on_loadVtyp3);
                            if (on_loadVtyp3 == -1) {
                                indexObj.init();
                                TREE_LOAD == false;
                            }
                            else {
                                if (TREE_LOAD == false) {
                                    indexObj.init();
                                    TREE_LOAD = false;
                                }
                                else {
                                    TREE_LOAD = false;
                                }
                            }
                        }
                        else {
                            $("#thumb_view_List").html(html_imageList); //append 3rd 
                            $("#tbTbody_TRCO_LIST").html(html_trcoList);
                        }
                    }
                    //					if(TREE_LOAD == false){
                    //						indexObj.init();
                    //						$("#myOrgArea").children().next().remove();
                    //						const dv = $("#SELECTED_DV").val();
                    //						checkViewType(dv);
                    //						TREE_LOAD = false;
                    //					}else{
                    //						TREE_LOAD = false;
                    //						
                    //					}
                    if (Clk_Vtp == "1") {
                        if (dat.EMPL_REC.length == 0) {
                            $(".result_none_wrap").show();
                            $("#srcImg").hide();
                            $("#noSrcImg").show();
                            if ($("#frmSearch").find("input[type=text]").is(":focus")) {
                                $(".list_table , .paging_wrap").hide();
                                $("#myOrgArea").hide();
                                $("#btn_org_tree").hide();
                                $(".schbx_r").show();
                                $(".combo_box4").hide();
                                $(".result_none_wrap").show();
                                $(".view_listwrap").show();
                                $(".view_list").show();
                                $(".combo_title").show();
                                $("#noSrcImg").hide();
                                $("#srcImg").show();
                            }
                            else if (srchTxt != "") {
                                $(".list_table , .paging_wrap").hide();
                                $("#myOrgArea").hide();
                                $("#btn_org_tree").hide();
                                $(".schbx_r").show();
                                $(".combo_box4").hide();
                                $(".result_none_wrap").show();
                                $(".view_listwrap").show();
                                $(".view_list").show();
                                $(".combo_title").show();
                                $("#noSrcImg").hide();
                                $("#srcImg").show();
                            }
                            else {
                                $(".result_none_wrap").show();
                                $("#myOrgArea").hide();
                                $("#btn_org_tree").hide();
                                $(".view_listwrap").show();
                                $(".view_list").show();
                                $(".combo_title").show();
                                $(".schbx_r").show();
                                $("#noSrcImg").show();
                                $("#srcImg").hide();
                                //change 29/04/2016
                                $(".list_table , .paging_wrap").hide();
                                $(".thumb_viewwrap").show();
                                $("#tab_list li").html("<li><span i18nCd='lang_all'>" + $.i18n.prop("lang_all") + "</span><span class=\"no\" id=\"list_all_cnt\">(" + dat.TOTL_RSLT_CNT + ")</span></li>");
                            }
                        }
                        else {
                            $(".result_none_wrap").hide();
                            $(".list_table , .paging_wrap").hide();
                            $(".thumb_viewwrap , .btn_moreview").hide();
                            $(".thumb_viewwrap").show();
                            $("#myOrgArea").hide();
                            $("#btn_org_tree").hide();
                            $(".schbx_r").show();
                            $(".view_listwrap").show();
                            $(".view_list").show();
                            $(".combo_title").show();
                        }
                        /*if(thumbTotal <= 30){
                            $(".btn_moreview").hide();
                        }else{
                            $(".btn_moreview").show();
                        };*/
                        //Thumb view defual list modify20160401
                        //count page than hide View Buttom
                        thumb_img_size = $("#thumb_view_List li").size();
                        $("#thumb_view_List li:lt(" + thumb_per_page + ")").show();
                        if (thumbTotal == thumb_img_size) {
                            $(".btn_moreview").hide();
                        }
                        else {
                            if (thumbTotal <= 30) {
                                $(".btn_moreview").hide();
                            }
                            else {
                                $(".btn_moreview").show();
                            }
                            ;
                        }
                        //onView Limited
                    }
                    else if (Clk_Vtp == "2") {
                        if (dat.EMPL_REC.length == 0) {
                            $("#grpPopupBtn").css("display", "none");
                            if ($("#frmSearch").find("input[type=text]").is(":focus")) {
                                $(".list_table , .paging_wrap").show();
                                $("#myOrgArea").hide();
                                $("#btn_org_tree").hide();
                                $(".schbx_r").show();
                                $(".view_listwrap").show();
                                $(".view_list").show();
                                $(".combo_title").show();
                                $(".result_none_wrap").hide();
                                $("#tbTbody_TRCO_LIST").html("<tr class=\"fir\"><td colspan=\"" + $("#tb_TRCO_LIST colgroup col").length + "\" style=\"text-align:center;\" i18nCd='lang_noList'>" + $.i18n.prop("lang_noList") + "</td></tr>");
                            }
                            else if (srchTxt != "") {
                                $(".list_table , .paging_wrap").show();
                                $("#myOrgArea").hide();
                                $("#btn_org_tree").hide();
                                $(".schbx_r").show();
                                $(".view_listwrap").show();
                                $(".view_list").show();
                                $(".combo_title").show();
                                $(".result_none_wrap").hide();
                                //change 29/04/2016		
                                $("#tab_list li").html("<li>'" + srchTxt + "' <span i18nCd='lang_searchEmpl'>" + $.i18n.prop("lang_searchEmpl") + "</span><span class=\"no\" id=\"list_all_cnt\">(" + dat.TOTL_RSLT_CNT + "명)</span></li>");
                                $("#tbTbody_TRCO_LIST").html("<tr class=\"fir\"><td colspan=\"" + $("#tb_TRCO_LIST colgroup col").length + "\" style=\"text-align:center;\" i18nCd='lang_noList'>" + $.i18n.prop("lang_noList") + "</td></tr>");
                            }
                            else {
                                $(".result_none_wrap").hide();
                                $("#myOrgArea").hide();
                                $("#btn_org_tree").hide();
                                $(".schbx_r").show();
                                $(".view_listwrap").show();
                                $(".view_list").show();
                                $(".combo_title").show();
                                //change 29/04/2016		
                                $("#tab_list li").html("<li><span i18nCd='lang_all'>" + $.i18n.prop("lang_all") + "</span><span class=\"no\" id=\"list_all_cnt\">(" + dat.TOTL_RSLT_CNT + ")</span></li>");
                                $("#tbTbody_TRCO_LIST").html("<tr class=\"fir\"><td colspan=\"" + $("#tb_TRCO_LIST colgroup col").length + "\" style=\"text-align:center;\" i18nCd='lang_noList'>" + $.i18n.prop("lang_noList") + "</td></tr>");
                                //change 29/04/2016			
                            }
                        }
                        else {
                            $("#grpPopupBtn").css("display", "block");
                            $(".result_none_wrap").hide();
                            $(".view_list").show();
                            $(".list_table , .paging_wrap").show();
                            $(".thumb_viewwrap , .btn_moreview").hide();
                            $("#btn_org_tree").hide();
                            $("#myOrgArea").hide();
                            $(".schbx_r").show();
                            $(".view_listwrap").show();
                            $(".combo_title").show();
                        }
                    }
                    else if (Clk_Vtp == "3") {
                        $("#myOrgArea").show();
                        $("#btn_org_tree").css("display", "inline-block");
                        //						if(tree_dvsn_cd != null || tree_dvsn_cd == "" || tree_dvsn_cd == "0"){
                        //							
                        //						}
                        //						else{
                        //							orgBusinessObj.selectNode(tree_dvsn_cd);//modify20160713
                        //						}
                        // 30-06-2016 show search box 
                        $(".schbx_r").hide();
                        $(".view_listwrap").hide();
                        $(".result_none_wrap").hide();
                        $(".view_list").hide();
                        $(".combo_title").hide();
                        $(".list_table , .paging_wrap").hide();
                        $(".thumb_viewwrap , .btn_moreview").hide();
                    }
                }
                else if (VType == "1") {
                    $(".btnbx_r a.btn_thumb").click();
                    /*$("#thumb_view_List").html(html_imageList);//table draw
                    $(".btnbx_r a.btn_thumb").addClass("on");
                    $(".list_table , .paging_wrap").hide();
                    $("#myOrgArea").hide();
                    $(".schbx_r").show();
                    $(".view_listwrap").show();
                    $(".view_list").show();
                    $(".combo_title").show();
                    
                    $(".thumb_viewwrap").show();
                    //Thumb view defual list modify20160401
                    thumb_img_size = $("#thumb_view_List li").size();
                    $("#thumb_view_List li:lt("+thumb_per_page+")").show();
                    //count page than hide View Buttom
                    if(thumbTotal <= 30){
                        $(".btn_moreview").hide();
                    }else{
                        $(".btn_moreview").show();
                    };*/
                }
                else if (VType == "2") {
                    $("#myOrgArea").hide();
                    $(".schbx_r").show();
                    $(".view_listwrap").show();
                    $(".view_list").show();
                    $(".combo_title").show();
                    $(".result_none_wrap").hide();
                    //_searchList.setPagePerCnt(10);
                    $(".btnbx_r a.btn_list").addClass("on");
                    $("#tbTbody_TRCO_LIST").html(html_trcoList); //©20160720
                    $(".btnbx_r a.btn_list").click();
                    if (dat.EMPL_REC.length == 0) {
                        if ($("#frmSearch").find("input[type=text]").is(":focus")) {
                            $(".list_table , .paging_wrap").show();
                            $(".combo_box4").hide();
                            $(".result_none_wrap").hide();
                            $("#tbTbody_TRCO_LIST").html("<tr class=\"fir\"><td colspan=\"" + $("#tb_TRCO_LIST colgroup col").length + "\" style=\"text-align:center;\" i18nCd='lang_noList'>" + $.i18n.prop("lang_noList") + "</td></tr>");
                        }
                        else if (srchTxt != "") {
                            $(".list_table , .paging_wrap").show();
                            $(".combo_box4").hide();
                            $(".result_none_wrap").hide();
                            $("#tbTbody_TRCO_LIST").html("<tr class=\"fir\"><td colspan=\"" + $("#tb_TRCO_LIST colgroup col").length + "\" style=\"text-align:center;\" i18nCd='lang_noList'>" + $.i18n.prop("lang_noList") + "</td></tr>");
                        }
                        else {
                            $(".result_none_wrap").hide();
                            $("#tbTbody_TRCO_LIST").html("<tr class=\"fir\"><td colspan=\"" + $("#tb_TRCO_LIST colgroup col").length + "\" style=\"text-align:center;\" i18nCd='lang_noList'>" + $.i18n.prop("lang_noList") + "</td></tr>");
                        }
                    }
                    else {
                        $(".result_none_wrap").hide();
                        $(".list_table , .paging_wrap").show();
                        $(".thumb_viewwrap , .btn_moreview").hide();
                    }
                    //testing draw pagination
                }
                else if (VType == "3") {
                    /*const dv = $("#SELECTED_DV").val();
                    checkViewType(dv);*/
                    /*$("#myOrgArea").empty();
                    indexObj.init();
                    */
                    $(".btnbx_r a.btn_org").click(); //added 20160713
                    $("#myOrgArea").show();
                    $(".btnbx_r a.btn_org").addClass("on");
                    $("#btn_org_tree").css("display", "inline-block");
                    $(".schbx_r").hide();
                    $(".result_none_wrap").hide();
                    $(".view_list").hide();
                    $(".view_listwrap").hide();
                    $(".combo_title").hide();
                    $(".list_table , .paging_wrap").hide();
                    $(".thumb_viewwrap , .btn_moreview").hide();
                    //TODO: call provided function
                }
            } //REC View type update 20160329
        }
        sf_grpCheck(); //그룹지정박스 설정 (목록조회시는 체크한 record가 없으므로 그룹지정박스는 초기화됨)
        //목록 체크박스 선택시에 그룹지정 체크박스 표시해주기
        $("input[name=trco]").bind("click", function () {
            sf_grpCheck();
        });
        //목록 체크박스 전체선택시 그룹지정 체크박스 표시해주기
        $("#allCheck").bind("click", function () {
            var allCheck = $(this).is(":checked");
            $("input[name=trco]").each(function () {
                $(this).attr("checked", allCheck);
            });
            sf_grpCheck();
        });
        //그룹지정레이어의 체크박스 설정
        function sf_grpCheck() {
            //기본그룹 
            $("#bscGrpSetList li").each(function () {
                $(this).find("img").attr("src", "../img/btn/btn_checkbox3.gif");
                sf_drawGrpCheck(this);
            });
            //사용자그룹 
            $("#userGrpSetList li").each(function () {
                $(this).find("img").attr("src", "../img/btn/btn_checkbox3.gif");
                sf_drawGrpCheck(this);
            });
        }
        //그룹지정레이어의 체크박스 설정
        function sf_drawGrpCheck(objGrp) {
            var grp_cd = "," + $(objGrp).find("a").attr("val-grpcd") + ",";
            var grpChkCnt = 0;
            $("input[name=trco]:checked").each(function () {
                var grp_cd_list = "," + $(this).parent().find("input[name=GRP_CD]").val() + ",";
                if (grp_cd_list.indexOf(grp_cd) > -1) {
                    grpChkCnt++;
                }
            });
            //해당그룹(_this)이 체크된 직원목록에 모두 없는경우
            if (grpChkCnt == 0) {
                $(objGrp).find("img").attr("src", "../img/btn/btn_checkbox3.gif");
                $(objGrp).find("a").attr("val-checkstatus", "3");
            }
            //해당그룹(_this)이 체크된 직원목록에 모두 있는경우
            else if (grpChkCnt == $("input[name=trco]:checked").length) {
                $(objGrp).find("img").attr("src", "../img/btn/btn_checkbox1.gif");
                $(objGrp).find("a").attr("val-checkstatus", "1");
            }
            //해당그룹(_this)이 체크된 직원목록에 일부분만 있는경우
            else {
                $(objGrp).find("img").attr("src", "../img/btn/btn_checkbox2.gif");
                $(objGrp).find("a").attr("val-checkstatus", "2");
            }
        }
        //직원 상세정보 열기
        $("#tb_TRCO_LIST tbody tr td").bind("click", function () {
            var idxTr = $("#tb_TRCO_LIST tbody tr").parent().find("tr").index($(this).parent());
            var idxTd = $("#tb_TRCO_LIST tbody tr:eq(" + idxTr + ") td").parent().find("td").index(this);
            var idxLastTd = $("#tb_TRCO_LIST tbody tr:eq(" + idxTr + ") td").length - 1;
            _curTrcoNo = _datTrcoList[idxTr].EMPL_IDNT_ID; //선택한 레코드 
            //체크박스
            if (idxTd == 0) {
                return true;
            }
            //즐겨찾기
            /*else if(idxTd == 1){
                if(_SUBMIT_YN == "N"){ //즐겨찾기 재실행 방지
                    return true;
                }
                
                _SUBMIT_YN = "N"; //즐겨찾기 재실행 방지
                updateBkmk(this);
            return true;
            }
            else if(idxTd == 1){
                if(_SUBMIT_YN == "N"){ //즐겨찾기 재실행 방지
                    return true;
                }
                
                _SUBMIT_YN = "N"; //즐겨찾기 재실행 방지
                updateBkmk(this);
                return true;
            }*/
            //컨텐츠사용여부
            /*else if(idxTd == idxLastTd){
                return true;
            }*/
            var del_yn = "N";
            if (_searchList.getInqyDsnc() == "4") { //휴지통
                del_yn = "Y";
            }
            else if (_searchList.getInqyDsnc() == "8") {
                del_yn = "Y";
            }
            else if (_searchList.getInqyDsnc() == "9") {
                del_yn = "Y";
            }
            if ($("#str_empl_idnt_id").val() == _datTrcoList[idxTr].EMPL_IDNT_ID) {
                var url_1 = "empl_1002_01.act?"
                    + "&EMPL_IDNT_ID=" + $("#str_empl_idnt_id").val()
                    + "&DEL_YN=N"
                    + "&MY_PROFILE=Y"
                    + "&_tmp="
                    + "&CLPH_NO_OPPB_YN=" + _emplVisibleDat.CLPH_NO_OPPB_YN
                    + "&EML_OPPB_YN=" + _emplVisibleDat.EML_OPPB_YN;
                parent.openEmplPopContent(url_1);
                return;
            }
            //상세정보 보기
            var url = "empl_1002_01.act?BSNN_NO=" + _datTrcoList[idxTr].BSNN_NO
                + "&EMPL_IDNT_ID=" + _datTrcoList[idxTr].EMPL_IDNT_ID
                + "&" + _searchList.getSearchString()
                + "&DEL_YN=" + del_yn
                + "&_tmp="
                + "&CLPH_NO_OPPB_YN=" + _emplVisibleDat.CLPH_NO_OPPB_YN
                + "&EML_OPPB_YN=" + _emplVisibleDat.EML_OPPB_YN;
            parent.openEmplPopContent(url);
        });
        //empl_detail_popup_for image view
        $("#thumb_view_List li .thumb_top, .thumb_info").bind("click", function ( /*event*/) {
            //event.preventDefault();
            var idxTr = $("#thumb_view_List li").index($(this).parents("li"));
            _curTrcoNo = _datTrcoList[idxTr].EMPL_IDNT_ID; //선택한 레코드 
            var del_yn = "N";
            if (_searchList.getInqyDsnc() == "4") { //휴지통
                del_yn = "Y";
            }
            if ($("#str_empl_idnt_id").val() == _datTrcoList[idxTr].EMPL_IDNT_ID) {
                var url_2 = "empl_1002_01.act?"
                    + "&EMPL_IDNT_ID=" + $("#str_empl_idnt_id").val()
                    + "&DEL_YN=N"
                    + "&MY_PROFILE=Y"
                    + "&_tmp="
                    + "&CLPH_NO_OPPB_YN=" + _emplVisibleDat.CLPH_NO_OPPB_YN
                    + "&EML_OPPB_YN=" + _emplVisibleDat.EML_OPPB_YN;
                parent.openEmplPopContent(url_2);
                return;
            }
            //const left_x;
            //상세정보 보기
            var url = "empl_1002_01.act?BSNN_NO=" + _datTrcoList[idxTr].BSNN_NO
                + "&EMPL_IDNT_ID=" + _datTrcoList[idxTr].EMPL_IDNT_ID
                + "&" + _searchList.getSearchString()
                + "&DEL_YN=" + del_yn
                + "&_tmp="
                + "&CLPH_NO_OPPB_YN=" + _emplVisibleDat.CLPH_NO_OPPB_YN
                + "&EML_OPPB_YN=" + _emplVisibleDat.EML_OPPB_YN;
            parent.openEmplPopContent(url);
            parent.$(".layer").next().remove();
            //				if($(window).width() < 1350){
            //					const x = $(this).parent().parent().position();
            //					const y = parent.$("html body").scrollTop();// $('.thumb_viewwrap')[0].scrollHeight;
            //					const xy= $(window).width()*y;
            //					
            //					if(x.left == 1){
            //						left_x = 375;
            //					}else if(x.left == 175){
            //						left_x = 550;
            //					}else if(x.left == 349){
            //						left_x = 725;
            //					}else if(x.left == 523){
            //						left_x = 900;
            //					}else if(x.left == 697){
            //						left_x = 513;
            //					}else if(x.left == 871){
            //						left_x = 688;
            //					}	
            //					parent.$("#layerPopupIfr").css({
            //						'left' : left_x,
            //						'top' : x.top - y + 46
            //					});					
            //				}else{
            //					const x = $(this).parent().parent().position();
            //					const y = parent.$("html body").scrollTop();// $('.thumb_viewwrap')[0].scrollHeight;
            //					
            //					if(x.left == 1){
            //						left_x = 375;
            //					}else if(x.left == 175){
            //						left_x = 550;
            //					}else if(x.left == 349){
            //						left_x = 725;
            //					}else if(x.left == 523){
            //						left_x = 900;
            //					}else if(x.left == 697){
            //						left_x = 1073;
            //					}else if(x.left == 871){
            //						left_x = 688;
            //					}else if(x.left == 1045){
            //						left_x = 859;
            //					}	
            //					parent.$("#layerPopupIfr").css({
            //						'left' : left_x,
            //						'top' : x.top - y + 46
            //					});
            //				}
        });
        //chen 20160725
        _searchList.setTotlPageNcnt(dat.TOTL_PAGE_NCNT);
        drawTablePaing("table_paging", drawTrcoList, dat.PAGE_NO, _searchList.getTotlPageNcnt());
        _loding.stop();
        ckScreenSize(this);
        parent.ifrMainResize("N", 20); // 20160721 borey
        //검색 포커스
        parent.setSchFocus();
        //관리자일 경우 등록버튼 보임
        var popupOpen = parent.getRcmdPopupOpen();
        if ($("#str_cnts_msgr_yn").attr("value") == "Y" && popupOpen) {
            //직원초대 기능안내 popup
            /*if($.cookie("COOKIE_RCMD_POPUP") != "close"){
                const rtnPop = postPop("","/empl_1007_01.act",543,351);
                rtnPop.focus();
            }*/
            parent.setRcmdPopupClose();
        }
    });
}
//프로필정보 
function drawProfile() {
    var jexAjax = jex.createAjaxUtil("empl_2000_01_r001");
    jexAjax.execute(function (dat) {
        var imgProfile = dat.PRFL_PHTG ? dat.PRFL_PHTG : "../img/img_lnb_nullphoto.gif";
        var dvsnName = dat.DVSN_NM;
        var userName = dat.FLNM;
        //const cmnm = dat.BSNN_NM;
        var cmnm = dat.USE_INTT_NM;
        $(".user_info_box").find("img").attr("src", imgProfile);
        $(".user_info_box").find("li:eq(0) strong").html(userName);
        $(".user_info_box").find("li:eq(1)").html(dvsnName);
        $("#totEmplBtn").find("a").html(cmnm);
        $("#totEmplBtn").find("a").attr("title", cmnm); //modify20160715
        if (_searchList.getInqyDsnc() != "4")
            $("#contentTitle h1").html(cmnm);
    });
}
var _arrBsunList;
var _arrDvsnList;
var _arrDvsnTreeOpen = [];
var _arrDvsnNm = [];
//기본그룹목록 표시
function drawBscGrpList(type) {
    //sf_drawTrcoGrpCnt();
    sf_drawBscGrpList();
    //부서목록 (weauth)
    function sf_drawBscGrpList() {
        var jexAjax = jex.createAjaxUtil("empl_2001_01_l001");
        jexAjax.execute(function (dat) {
            var respData = JSON.parse(dat.RESP_DATA);
            //_arrDvsnList = respData.REC;
            _arrBsunList = respData;
            sf_drawTrcoGrpCnt();
        });
    }
    //부서목록 (직원관리)
    function sf_drawTrcoGrpCnt() {
        var jexAjax = jex.createAjaxUtil("empl_2001_01_r001");
        jexAjax.execute(function (dat) {
            var dvsnlist_empl = dat.REC;
            var dvsnlist_weauth = _arrBsunList;
            _arrDvsnList = [];
            //전체 직원 개수
            var totEmplCnt = 0;
            for (var j = 0; j < dvsnlist_empl.length; j++) {
                totEmplCnt += parseInt(dvsnlist_empl[j].DVSN_CNT);
            }
            $("#totEmplBtn").find("strong").html(totEmplCnt);
            /*if(dat.REC.length == 0 && isAdmin){
                parent.ifr_main.openIfrContent("empl_1011_01.act");
            }*/
            //부서,직책,직급이 없다면 안래 레이어 출력
            dsvn_use_cd_view();
            //여러사업장인 이용기관일 경우
            //if(dvsnlist_weauth.length > 1) //multi_bsun
            if (false) {
                var maxLvl = 0; //최하 뎁스
                //weauth부서와 empl 부서의 정보(부서수) 합치기
                for (var z = 0; z < dvsnlist_weauth.length; z++) {
                    var tmpBsuncd = dvsnlist_weauth[z].BSUN_ID;
                    var tmpBsnnNo = dvsnlist_weauth[z].BSNN_NO;
                    var tmpArrDvsn = dvsnlist_weauth[z].REC;
                    for (var k = 0; k < tmpArrDvsn.length; k++) {
                        var dvsnCnt = "0";
                        for (var j = 0; j < dvsnlist_empl.length; j++) {
                            //사업장 단위로 부서코드 체크 (부서코드가 사업장 상관없이 중복되지는 않지만 데이타 꼬임 방지...)
                            if (null2void(tmpBsnnNo) == null2void(dvsnlist_empl[j].BSNN_NO) && null2void(tmpArrDvsn[k].DVSN_CD) == null2void(dvsnlist_empl[j].DVSN_CD)) {
                                dvsnCnt = null2void(dvsnlist_empl[j].DVSN_CNT, "0");
                                break;
                            }
                        }
                        tmpArrDvsn[k]["DVSN_CNT"] = dvsnCnt;
                        tmpArrDvsn[k]["BSNN_NO"] = tmpBsnnNo;
                        if (parseInt(tmpArrDvsn[k].LVL) > maxLvl) {
                            maxLvl = parseInt(tmpArrDvsn[k].LVL);
                        }
                    }
                    dvsnlist_weauth[z]["maxLvl"] = maxLvl;
                    maxLvl = 0;
                    var tmtUseDvsnCnt = 0;
                    var tmpTotDvsnCnt = 0;
                    //해당사업장의 부서 사용직원수
                    for (var k = 0; k < tmpArrDvsn.length; k++) {
                        tmtUseDvsnCnt += parseInt(tmpArrDvsn[k]["DVSN_CNT"]);
                    }
                    //해당사업장의 전체직원 수
                    for (var j = 0; j < dvsnlist_empl.length; j++) {
                        if (null2void(dvsnlist_weauth[z].BSNN_NO) != "" && null2void(dvsnlist_weauth[z].BSNN_NO) == null2void(dvsnlist_empl[j].BSNN_NO)) {
                            tmpTotDvsnCnt += parseInt(null2void(dvsnlist_empl[j].DVSN_CNT, "0"));
                        }
                    }
                    if (tmpTotDvsnCnt - tmtUseDvsnCnt > 0) {
                        tmpArrDvsn.push({
                            "BSNN_NO": tmpBsnnNo,
                            "BSUN_CD": tmpBsuncd,
                            "DVSN_CD": "",
                            "DVSN_NM": "부서미지정",
                            "HGRN_DVSN_CD": "",
                            "DVSN_DSNC1": "",
                            "DVSN_DSNC2": "",
                            "LVL": "0",
                            "DVSN_MNGR_NM": "",
                            "DVSN_CNT": (tmpTotDvsnCnt - tmtUseDvsnCnt)
                        });
                    }
                }
                var arrBsunList = [];
                //사업장별 부서 정리
                for (var z = 0; z < dvsnlist_weauth.length; z++) {
                    var bsunRec = {};
                    bsunRec["BSNN_NO"] = dvsnlist_weauth[z].BSNN_NO;
                    bsunRec["BSUN_CD"] = dvsnlist_weauth[z].BSUN_ID;
                    bsunRec["BSNN_NM"] = dvsnlist_weauth[z].BSNN_NM;
                    bsunRec["maxLvl"] = dvsnlist_weauth[z].maxLvl;
                    bsunRec["dvsnList"] = [];
                    //뎁스별 array 분리 
                    var arrDepthList = [];
                    var dvsnMaxLvl = dvsnlist_weauth[z].maxLvl;
                    var dvsnRec = dvsnlist_weauth[z].REC;
                    for (var k = 0; k <= dvsnMaxLvl; k++) {
                        var arrDepthGrp = {};
                        for (var i = 0; i < dvsnRec.length; i++) {
                            if (dvsnRec[i].LVL != k + "")
                                continue;
                            var hgrnDvsnCd = k == 0 ? "" : "" + dvsnRec[i].HGRN_DVSN_CD;
                            if (!arrDepthGrp[hgrnDvsnCd]) {
                                arrDepthGrp[hgrnDvsnCd] = [];
                            }
                            arrDepthGrp[hgrnDvsnCd].push(dvsnRec[i]);
                        }
                        arrDepthList.push(arrDepthGrp);
                    }
                    //하위뎁스의 부서목록 수에 따라 상위 부서 수 update 처리 (상위부서에서 자신의부서수와 하위부서 목록수까지 더한 수를 표시해줌)
                    for (var k = dvsnMaxLvl; k >= 0; k--) {
                        var arrGrp = arrDepthList[k];
                        for (var j in arrGrp) {
                            var sub_key = j; //부모코드
                            var sub_val = arrGrp[j]; //부모에 대한 자식목록
                            var dvsnTotCnt = 0; //자식목록의 부서수 총 합계 저장변수
                            //부모(sub_key)에 대한 자식 부서수 sum 구하기
                            for (var i = 0; i < sub_val.length; i++) {
                                dvsnTotCnt += parseInt(sub_val[i].DVSN_CNT);
                            }
                            //부모(sub_key)의 부서수(DVSN_CNT) 값  update
                            var arrGrpParent = arrDepthList[k - 1];
                            for (var x in arrGrpParent) {
                                //const sub_key2 = x;			
                                var sub_val2 = arrGrpParent[x];
                                for (var xx = 0; xx < sub_val2.length; xx++) {
                                    if (sub_key == sub_val2[xx].DVSN_CD) {
                                        sub_val2[xx].DVSN_CNT = parseInt(sub_val2[xx].DVSN_CNT) + dvsnTotCnt;
                                        break;
                                    }
                                }
                            }
                            if (k == 0)
                                bsunRec["DVSN_CNT"] = dvsnTotCnt;
                        }
                    }
                    bsunRec["dvsnList"] = arrDepthList;
                    bsunRec["DVSN_CNT"] = null2void(bsunRec["DVSN_CNT"], "0");
                    arrBsunList.push(bsunRec);
                }
                //목록 그리기
                $("#bscGrpList").html("");
                var _loop_1 = function (z) {
                    var html_1 = "";
                    html_1 += "\n	<li class=\"menu1_box off\" val-dvsncd=\"" + "-1" + "\" val-bsnnno=\"" + arrBsunList[z].BSNN_NO + "\">";
                    html_1 += "\n		<div class=\"menu1\">";
                    if (arrBsunList[z].DVSN_CNT + "" == "0") {
                        html_1 += "\n			<a href=\"#none\"><img src=\"../img/bg/tree_none.gif\" alt=\"\"></a>";
                    }
                    else {
                        html_1 += "\n			<a href=\"#none\"><img src=\"../img/bg/tree_p.gif\" alt=\"\"></a>";
                    }
                    html_1 += "\n			<a href=\"#none\" class=\"txt\" onclick=\"checkViewType('" + arrBsunList[z].BSNN_NO + "')\">" + arrBsunList[z].BSNN_NM + "</a> <span class=\"badge2\"><strong>" + arrBsunList[z].DVSN_CNT + "</strong></span>";
                    html_1 += "\n		</div>";
                    html_1 += "\n	</li>";
                    $("#bscGrpList").append(html_1);
                    var dvsnMaxLvl = arrBsunList[z].maxLvl;
                    var tmpBsnnno = arrBsunList[z].BSNN_NO;
                    var arrTotList = arrBsunList[z].dvsnList;
                    var _loop_3 = function (k) {
                        var arrGrp = arrTotList[k];
                        var _loop_4 = function (j) {
                            var sub_key = j;
                            var sub_val = arrGrp[j];
                            var isChild = false;
                            html_1 = "";
                            if (k == 0) {
                                isChild = fn_arrIndexOf(_arrDvsnTreeOpen, "-1_" + tmpBsnnno) > -1 ? true : false;
                            }
                            else {
                                isChild = fn_arrIndexOf(_arrDvsnTreeOpen, sub_key) > -1 ? true : false;
                            }
                            var addClass = k == 0 ? "class=\"menu_2depth\"" : "";
                            if (isChild) {
                                html_1 += "\n		<ul " + addClass + " style=\"display:block;\">";
                            }
                            else {
                                html_1 += "\n		<ul " + addClass + " style=\"display:none;\">";
                            }
                            for (var i = 0; i < sub_val.length; i++) {
                                html_1 += "\n		<li class=\"\" val-dvsncd=\"" + sub_val[i].DVSN_CD + "\" val-bsnnno=\"" + sub_val[i].BSNN_NO + "\">";
                                html_1 += "\n		<div >";
                                html_1 += "\n			<a href=\"#none\"><img src=\"../img/bg/tree_none.gif\" alt=\"\"></a>";
                                html_1 += "\n			<a href=\"#none\" onclick=\"checkViewType(" + sub_val[i].BSNN_NO + ",'" + sub_val[i].DVSN_NM + "')\">" + sub_val[i].DVSN_NM + "<strong>" + sub_val[i].DVSN_CNT + "</strong></a>";
                                html_1 += "\n		</div>";
                                html_1 += "\n		</li>";
                            }
                            html_1 += "\n		</ul>";
                            $("#bscGrpList").find("li").each(function () {
                                //if(sub_key == $(this).attr("val-dvsncd"))
                                if (k == 0 && tmpBsnnno == $(this).attr("val-bsnnno")) {
                                    $(this).find("div").after(html_1);
                                }
                                else if (k != 0 && sub_key == $(this).attr("val-dvsncd")) {
                                    $(this).find("a:eq(1)").after(html_1);
                                }
                                else {
                                    return true;
                                }
                                if (isChild) {
                                    $(this).find("img:eq(0)").attr("src", "../img/bg/tree_m.gif"); //부모트리 - 로 변경
                                }
                                else {
                                    $(this).find("img:eq(0)").attr("src", "../img/bg/tree_p.gif"); //부모트리 + 로 변경
                                }
                            });
                        };
                        for (var j in arrGrp) {
                            _loop_4(j);
                        }
                    };
                    for (var k = 0; k <= dvsnMaxLvl; k++) {
                        _loop_3(k);
                    }
                };
                for (var z = 0; z < arrBsunList.length; z++) {
                    _loop_1(z);
                }
                //미지정건 표시
                var nullDvsnCnt = totEmplCnt;
                var useDvsnCnt = 0;
                for (var z = 0; z < arrBsunList.length; z++) {
                    useDvsnCnt += parseInt(arrBsunList[z].DVSN_CNT);
                }
                nullDvsnCnt = totEmplCnt - useDvsnCnt;
                var html = "";
                if (nullDvsnCnt > 0) {
                    html += "<li class=\"menu1_box\" val-dvsncd=\"\">";
                    html += "	<div class=\"menu1\">";
                    html += "		<a href=\"#none\"><img src=\"../img/bg/tree_none.gif\" alt=\"\"></a>";
                    html += "		<a href=\"#none\" class=\"txt\" i18nCd='lang_undefinedDept'>" + $.i18n.prop("lang_undefinedDept") + "</a> <span class=\"badge2\"><strong>" + nullDvsnCnt + "</strong></span>";
                    html += "	</div>";
                    html += "</li>";
                }
                $("#bscGrpList").append(html);
            }
            //사업장 1개라면                                                        //multi_bsun
            //else if(dvsnlist_weauth.length == 1)  //multi_bsun
            else if (dvsnlist_weauth.length > 0) {
                //const tmpBssnnno = dvsnlist_weauth[0].BSNN_NO;  //multi_bsun
                //dvsnlist_weauth = dvsnlist_weauth[0].REC;     //multi_bsun
                var tmpBssnnno = ""; //1 busn
                var maxLvl = 0; //최하 뎁스
                //weauth부서와 empl 부서의 정보 합치기
                for (var i = 0; i < dvsnlist_weauth.length; i++) {
                    var dvsnCnt = "0";
                    for (var j = 0; j < dvsnlist_empl.length; j++) {
                        if (null2void(dvsnlist_weauth[i].DVSN_CD) == null2void(dvsnlist_empl[j].DVSN_CD)) {
                            dvsnCnt = null2void(dvsnlist_empl[j].DVSN_CNT, "0");
                            break;
                        }
                    }
                    dvsnlist_weauth[i]["DVSN_CNT"] = dvsnCnt;
                    dvsnlist_weauth[i]["BSNN_NO"] = tmpBssnnno;
                    if (parseInt(dvsnlist_weauth[i].LVL) > maxLvl) {
                        maxLvl = parseInt(dvsnlist_weauth[i].LVL);
                    }
                }
                //뎁스별 array 분리 
                var arrDepthList = [];
                var dvsnMaxLvl = maxLvl;
                var dvsnRec = dvsnlist_weauth;
                for (var k = 0; k <= dvsnMaxLvl; k++) {
                    var arrDepthGrp = {};
                    for (var i = 0; i < dvsnRec.length; i++) {
                        if (dvsnRec[i].LVL != k + "")
                            continue;
                        var hgrnDvsnCd = k == 0 ? "" : "" + dvsnRec[i].HGRN_DVSN_CD;
                        if (!arrDepthGrp[hgrnDvsnCd]) {
                            arrDepthGrp[hgrnDvsnCd] = [];
                        }
                        arrDepthGrp[hgrnDvsnCd].push(dvsnRec[i]);
                    }
                    arrDepthList.push(arrDepthGrp);
                }
                //하위뎁스의 부서목록 수에 따라 상위 부서 수 update 처리 (상위부서에서 자신의부서수와 하위부서 목록수까지 더한 수를 표시해줌)
                for (var k = dvsnMaxLvl; k >= 0; k--) {
                    var arrGrp = arrDepthList[k];
                    for (var j in arrGrp) {
                        var sub_key = j; //부모코드
                        var sub_val = arrGrp[j]; //부모에 대한 자식목록
                        var dvsnTotCnt = 0; //자식목록의 부서수 총 합계 저장변수
                        //부모(sub_key)에 대한 자식 부서수 sum 구하기
                        for (var i = 0; i < sub_val.length; i++) {
                            dvsnTotCnt += parseInt(sub_val[i].DVSN_CNT);
                        }
                        //부모(sub_key)의 부서수(DVSN_CNT) 값  update
                        var arrGrpParent = arrDepthList[k - 1];
                        for (var x in arrGrpParent) {
                            //const sub_key2 = x;			
                            var sub_val2 = arrGrpParent[x];
                            for (var xx = 0; xx < sub_val2.length; xx++) {
                                if (sub_key == sub_val2[xx].DVSN_CD) {
                                    sub_val2[xx].DVSN_CNT = parseInt(sub_val2[xx].DVSN_CNT) + dvsnTotCnt;
                                    break;
                                }
                            }
                        }
                    }
                }
                //목록 그리기
                $("#bscGrpList").html("");
                var arrTotList = arrDepthList;
                var _loop_2 = function (k) {
                    var html_2 = "";
                    var arrGrp = arrTotList[k];
                    var _loop_5 = function (j) {
                        var sub_key = j;
                        var sub_val = arrGrp[j];
                        if (k == 0) {
                            for (var i = 0; i < sub_val.length; i++) {
                                html_2 += "\n	<li class=\"menu1_box off\" val-dvsncd=\"" + sub_val[i].DVSN_CD + "\" val-bsnnno=\"" + sub_val[i].BSNN_NO + "\">";
                                html_2 += "\n		<div class=\"menu1\">";
                                html_2 += "\n			<a href=\"#none\"><img src=\"../img/bg/tree_none.gif\" alt=\"\"></a>";
                                /*html += "\n			<a href=\"#none\"  class=\"txt\" onclick=\"checkViewType('" + sub_val[i].DVSN_CD + "')\"  >"+sub_val[i].DVSN_NM+"</a> <span class=\"badge2\"><strong>"+sub_val[i].DVSN_CNT+"</strong></span>";*/
                                html_2 += "\n			<a href=\"#none\" class=\"txt\" onclick=\"checkViewType('" + sub_val[i].DVSN_CD + "')\" >" + sub_val[i].DVSN_NM + "</a> <span class=\"badge2\"><strong>" + sub_val[i].DVSN_CNT + "</strong></span>";
                                html_2 += "\n		</div>";
                                html_2 += "\n	</li>";
                            }
                        }
                        else if (k == 1) {
                            var isChild_1 = fn_arrIndexOf(_arrDvsnTreeOpen, sub_key) > -1 ? true : false;
                            html_2 = "";
                            if (isChild_1) {
                                html_2 += "\n		<ul class=\"menu_2depth\" style=\"display:block;\">";
                            }
                            else {
                                html_2 += "\n		<ul class=\"menu_2depth\" style=\"display:none;\">";
                            }
                            for (var i = 0; i < sub_val.length; i++) {
                                html_2 += "\n		<li class=\"\" val-dvsncd=\"" + sub_val[i].DVSN_CD + "\" val-bsnnno=\"" + sub_val[i].BSNN_NO + "\">";
                                html_2 += "\n		<div >";
                                html_2 += "\n			<a href=\"#none\"><img src=\"../img/bg/tree_none.gif\" alt=\"\"></a>";
                                html_2 += "\n			<a href=\"#none\" onclick=\"checkViewType('" + sub_val[i].DVSN_CD + "')\" >" + sub_val[i].DVSN_NM + "<strong>" + sub_val[i].DVSN_CNT + "</strong></a>";
                                html_2 += "\n		</div>";
                                html_2 += "\n		</li>";
                            }
                            html_2 += "\n		</ul>";
                            $("#bscGrpList").find("li").each(function () {
                                if (sub_key == $(this).attr("val-dvsncd")) {
                                    $(this).find("div").after(html_2);
                                    if (isChild_1) {
                                        $(this).find("img:eq(0)").attr("src", "../img/bg/tree_m.gif"); //부모트리 - 로 변경
                                    }
                                    else {
                                        $(this).find("img:eq(0)").attr("src", "../img/bg/tree_p.gif"); //부모트리 + 로 변경
                                    }
                                }
                            });
                        }
                        else {
                            var isChild_2 = fn_arrIndexOf(_arrDvsnTreeOpen, sub_key) > -1 ? true : false;
                            html_2 = "";
                            if (isChild_2) {
                                html_2 += "\n		<ul style=\"display:block;\">";
                            }
                            else {
                                html_2 += "\n		<ul style=\"display:none;\">";
                            }
                            for (var i = 0; i < sub_val.length; i++) {
                                html_2 += "\n			<li class=\"\" val-dvsncd=\"" + sub_val[i].DVSN_CD + "\" val-bsnnno=\"" + sub_val[i].BSNN_NO + "\">";
                                html_2 += "\n		<div >";
                                html_2 += "\n				<a href=\"#none\"><img src=\"../img/bg/tree_none.gif\" alt=\"\"></a>";
                                html_2 += "\n				<a href=\"#none\" onclick=\"checkViewType('" + sub_val[i].DVSN_CD + "')\" >" + sub_val[i].DVSN_NM + "<strong>" + sub_val[i].DVSN_CNT + "</strong></a>";
                                html_2 += "\n		</div>";
                                html_2 += "\n			</li>";
                            }
                            html_2 += "\n			</ul>";
                            $("#bscGrpList").find("li").each(function () {
                                if (sub_key == $(this).attr("val-dvsncd")) {
                                    $(this).find("a:eq(1)").after(html_2);
                                    if (isChild_2) {
                                        $(this).find("img:eq(0)").attr("src", "../img/bg/tree_m.gif"); //부모트리 - 로 변경
                                    }
                                    else {
                                        $(this).find("img:eq(0)").attr("src", "../img/bg/tree_p.gif"); //부모트리 + 로 변경
                                    }
                                }
                            });
                        }
                    };
                    for (var j in arrGrp) {
                        _loop_5(j);
                    }
                    if (k == 0)
                        $("#bscGrpList").append(html_2);
                };
                for (var k = 0; k <= maxLvl; k++) {
                    _loop_2(k);
                }
                //미지정건 표시
                var nullDvsnCnt = totEmplCnt;
                //부서목록이 ''값은 미지정
                //for(const i=0; i < dvsnlist_empl.length; i++){
                //	if(null2void(dvsnlist_empl[i].DVSN_CD) == ""){
                //		nullDvsnCnt += parseInt(dvsnlist_empl[i].DVSN_CNT);
                //	}
                //}
                var useDvsnCnt = 0;
                //weauth의 부서와 DB의 부서정보가 다를 수 있어서 ''로 비교하지 않고 실제Tree에 셋팅된값을 가져옴
                if (arrDepthList.length > 0 && JSON.stringify(arrDepthList) != "[{}]") {
                    var arrDepth1 = arrDepthList[0][""];
                    for (var i = 0; i < arrDepth1.length; i++) {
                        useDvsnCnt += parseInt(arrDepth1[i].DVSN_CNT);
                    }
                    nullDvsnCnt = totEmplCnt - useDvsnCnt;
                }
                var html = "";
                if (nullDvsnCnt > 0) {
                    html += "<li class=\"menu1_box\" val-dvsncd=\"\">";
                    html += "	<div class=\"menu1\">";
                    html += "		<a href=\"#none\"><img src=\"../img/bg/tree_none.gif\" alt=\"\"></a>";
                    html += "		<a href=\"#none\" class=\"txt\" i18nCd='lang_undefinedDept'>" + $.i18n.prop("lang_undefinedDept") + "</a> <span class=\"badge2\"><strong>" + nullDvsnCnt + "</strong></span>";
                    html += "	</div>";
                    html += "</li>";
                }
                $("#bscGrpList").append(html); //화면 왼쪽 기본그룹목록 표시 
            }
            //사업장정보가 없다면
            else if (dvsnlist_weauth.length == 0) {
                var html = "";
                if (totEmplCnt > 0) {
                    html += "<li class=\"menu1_box\" val-dvsncd=\"\">";
                    html += "	<div class=\"menu1\">";
                    html += "		<a href=\"#none\"><img src=\"../img/bg/tree_none.gif\" alt=\"\"></a>";
                    html += "		<a href=\"#none\" class=\"txt\" i18nCd='lang_undefinedDept'>" + $.i18n.prop("lang_undefinedDept") + "</a> <span class=\"badge2\"><strong>" + totEmplCnt + "</strong></span>";
                    html += "	</div>";
                    html += "</li>";
                }
                $("#bscGrpList").html(html); //화면 왼쪽 기본그룹목록 표시 
            }
            $('.scroll_tree_box').jScrollPane({
                autoReinitialise: true,
                verticalDragMinHeight: 20,
                verticalDragMaxHeight: 20,
                horizontalDragMinWidth: 20,
                horizontalDragMaxWidth: 20,
                hideFocus: true
            });
            // 20160526
            $("#bscGrpList li a").click(function () {
                $(".view_listwrap").css("display", "block");
                $(".tab_schbx").css("display", "none");
                //daravuth 07.27.2016
                excludeEmpl = false;
                thumb_per_page = 30; // 07.28.2016
                on_loadVtyp3++; //added 20160713
                TREE_LOAD = true; //added 20160713
                var _chkV_type = $("#VIEW_TYPE_CD").val(); //added 20160722 chen 
                tree_dvsn_cd = $(this).parent().parent().attr('val-dvsncd');
                $("#SELECTED_DV").val(tree_dvsn_cd);
                $("form#frmSearch").find("input").val("");
                var li_this = $(this).parent();
                if ($(li_this)[0].tagName == "DIV") {
                    li_this = $(this).parent().parent();
                }
                var idxA = $(li_this).find("a").index(this);
                if (idxA == 0) {
                    //트리 ■ 상태일 떄는 클릭안됨
                    if ($(this).find("img").attr("src").indexOf("../img/bg/tree_none.gif") == -1) {
                        if ($(this).find("img").attr("src").indexOf("../img/bg/tree_p.gif") > -1) {
                            $(this).find("img").attr("src", "../img/bg/tree_m.gif");
                            $(li_this).find("ul:eq(0)").show();
                            var arrDvsnTreeOpen = _arrDvsnTreeOpen;
                            var val_dvsncd = $(li_this).attr("val-dvsncd");
                            if (val_dvsncd == "-1") {
                                val_dvsncd = val_dvsncd + "_" + $(li_this).attr("val-bsnnno"); //사업장을 클릭한 경우
                            }
                            var openIdx = fn_arrIndexOf(arrDvsnTreeOpen, val_dvsncd);
                            if (openIdx == -1) {
                                arrDvsnTreeOpen[arrDvsnTreeOpen.length] = val_dvsncd;
                            }
                        }
                        else {
                            $(this).find("img").attr("src", "../img/bg/tree_p.gif");
                            $(li_this).find("ul:eq(0)").hide();
                            var arrDvsnTreeOpen = _arrDvsnTreeOpen;
                            var val_dvsncd = $(li_this).attr("val-dvsncd");
                            if (val_dvsncd == "-1") {
                                val_dvsncd = val_dvsncd + "_" + $(li_this).attr("val-bsnnno"); //사업장을 클릭한 경우
                            }
                            var openIdx = fn_arrIndexOf(arrDvsnTreeOpen, val_dvsncd);
                            if (openIdx > -1) {
                                arrDvsnTreeOpen.splice(openIdx, 1);
                            }
                        }
                    }
                }
                //직원 목록 열기
                else if (idxA == 1) {
                    var a_this = $(this);
                    //$("#bscGrpList li").each(function(){
                    //	$(this).removeClass("on").addClass("off");
                    //});
                    //$(li_this).removeClass("off").addClass("on");
                    $("#bscGrpList li a").each(function () {
                        $(this).css({ "color": "#cccccc", "font-weight": "normal" });
                    });
                    $(this).css({ "color": "#ffffff", "font-weight": "bold" });
                    $("#bscGrpList li").removeClass("SelectedDpt"); //new20160121 chen
                    $(this).parent().parent().addClass("SelectedDpt"); //new20160121 chen
                    _tmpCurGrpNm = $(a_this).clone().find("strong").remove().end().html();
                    $("#contentTitle h1").html(_tmpCurGrpNm);
                    closeIfrContent2();
                    leftMenuAllOff();
                    //$(this).parent().addClass("on");
                    var dvsnCdList_1 = "'" + $(li_this).attr("val-dvsncd") + "'";
                    $(li_this).find("li").each(function () {
                        dvsnCdList_1 += ",'" + $(this).attr("val-dvsncd") + "'";
                    });
                    //미지정, 부서미지정 이라면 부서코드목록이 없음
                    if (dvsnCdList_1 == "''") {
                        dvsnCdList_1 = "";
                        //미지정 
                        if ($(li_this).attr("class").indexOf("menu1_box") > -1) {
                            //이용기관명 클릭인 경우
                            //if($(li_this).attr("val-dvsncd") == "-1"){
                            //	_searchList.setInqyDsnc("2");
                            //	_searchList.setBsnnNo($(li_this).attr("val-bsnnno"));
                            //}
                            //미지정 클릭인 경우
                            //else{
                            $("#bscGrpList").find("li").each(function () {
                                if ($(this).attr("val-dvsncd") != "")
                                    dvsnCdList_1 += ",'" + $(this).attr("val-dvsncd") + "'";
                            });
                            var notInBsnnno_1 = "";
                            $("#bscGrpList").find("li[val-dvsncd=-1]").each(function () {
                                if ($(this).attr("val-bsnnno") != "")
                                    notInBsnnno_1 += ",'" + $(this).attr("val-bsnnno") + "'";
                            });
                            if (notInBsnnno_1 != "")
                                notInBsnnno_1 = notInBsnnno_1.substring(1);
                            if (dvsnCdList_1 != "")
                                dvsnCdList_1 = dvsnCdList_1.substring(1);
                            _searchList.setInqyDsnc("6");
                            _searchList.setBsnnNo(notInBsnnno_1);
                            //}
                            //Todo next block20161022
                            if (_chkV_type == "3") {
                                $("#VIEW_TYPE_CD_DF").val("3");
                                $(".btnbx_r a.btn_list").click(); //chen 20161022
                            }
                        }
                        //사업장밑에 부서미지정 일 경우
                        else {
                            $("#bscGrpList").find("li[val-dvsncd=-1]").each(function () {
                                if ($(this).attr("val-bsnnno") == $(li_this).attr("val-bsnnno")) {
                                    $(this).find("li").each(function () {
                                        if ($(this).attr("val-dvsncd") != "")
                                            dvsnCdList_1 += ",'" + $(this).attr("val-dvsncd") + "'";
                                    });
                                    return false;
                                }
                            });
                            if (dvsnCdList_1 != "")
                                dvsnCdList_1 = dvsnCdList_1.substring(1);
                            _searchList.setInqyDsnc("7");
                            _searchList.setBsnnNo($(li_this).attr("val-bsnnno"));
                        }
                        //checking 97q9579341
                        if (_chkV_type == "3") {
                            $(".btnbx_r a.btn_list").click(); //chen 20160728
                        }
                    }
                    else {
                        _searchList.setInqyDsnc("2");
                        _searchList.setBsnnNo($(li_this).attr("val-bsnnno"));
                    }
                    //_searchList.setGrpCd($(li_this).attr("val-dvsncd"));
                    _searchList.setGrpCd(dvsnCdList_1);
                    _searchList.setPageNo(1);
                    _searchList.setSrchWord("");
                    parent.setSearchTextDefault();
                    var Df_SetupV = $("#Df_SetupV").val();
                    if (Df_SetupV == "true") {
                        //modify 20160722 chen 
                        if (_chkV_type == "1") {
                            $(".btnbx_r a.btn_thumb").click(); //chen 20160728
                        }
                        else if (_chkV_type == "2") {
                            $(".btnbx_r a.btn_list").click(); //chen 20160728
                        }
                        else if (_chkV_type == "3") {
                            $(".btnbx_r a.btn_org").click(); //chen 20160728
                        }
                        $("#Df_SetupV").val("false");
                    }
                    //modify 20160722 chen 
                    if (_chkV_type == "1") {
                        _searchList.setPagePerCnt(30);
                    }
                    else if (_chkV_type == "2") {
                        _searchList.setPagePerCnt(10);
                        $("#combo_row").text(_searchList.getPagePerCnt());
                        $("#pagingPopBtn a:eq(0)").html("<span><em class=\"icon_list\" style=\"display:none;\"></em>" + _searchList.getPagePerCnt() + "</span>");
                    }
                    else if (_chkV_type == "3") {
                    }
                    _INQ_JOIN_DV = "";
                    drawTrcoList(); //직원 목록 표시
                    //loadDefaultConfig();
                    //drawBscGrpList();			//기본그룹 목록 표시
                    //drawUserGrpList();			//사용자그룹 목록 표시		
                }
            });
        });
    }
}
function testHtml2() {
    /*const html3 = "";
    const jexAjax = jex.createAjaxUtil("empl_3001_01_l001");
    jexAjax.execute(function(dat) {
        for(const i=0; i < dat.REC.length; i++){
            
            html3 += "    <li style=\"width:100px;overflow:hidden;text-overflow:ellipsis;-o-text-overflow:ellipsis;white-space:nowrap;display:block;\"><a href=\"#none\" val-grpcd=\""+dat.REC[i].GRP_CD+"\"><img src=\"../img/btn/btn_checkbox3.gif\" alt=\"\" /></a> "+dat.REC[i].GRP_NM+"</li>";
        }
        $("#userGrpSetList").empty().append(html3);	//그룹지정 > 사용자그룹목록 표시
        $("input[name=trco]").each(function(){
            $(this).attr("checked", "check");
        });
    });*/
    drawUserGrpList();
    $("input[name=trco]").each(function () {
        if ($(this).is(':checked')) {
            $(this).prop('checked', false);
        }
    });
    /*
        $.fn.exchangePositionWith = function(selector) {
            const other = $(selector);
            this.after(other.clone());
            other.after(this).remove();
        };*/
    //sf_grpCheck(); //그룹지정박스 설정 (목록조회시는 체크한 record가 없으므로 그룹지정박스는 초기화됨)
}
//사용자그룹목록 표시
function drawUserGrpList() {
    var jexAjax = jex.createAjaxUtil("empl_3001_01_l001");
    jexAjax.execute(function (dat) {
        if (_searchList.getInqyDsnc() == "3" && _searchList.getGrpCd() == "") {
            $("#userGrpDefault").find("dt").removeClass("off").addClass("on");
        }
        else {
            $("#userGrpDefault").find("dt").removeClass("on").addClass("off");
        }
        $("#userGrpTotCnt").html(dat.TOTL_RSLT_CNT); //나의그룹 수
        //$("#userBkmkCnt").html(dat.TOTL_BKMK_CNT);   //나의그룹 > 즐겨찾기 수 표시
        var html = "";
        for (var i = 0; i < dat.REC.length; i++) {
            var liClass = "off";
            if (_searchList.getInqyDsnc() == "3" && _searchList.getGrpCd() == dat.REC[i].GRP_CD) {
                liClass = "on";
            }
            //const firstDD = i==0?"first":"";
            var firstDD = i == 0 ? "" : "";
            html += "\n    <dd class=\"" + liClass + "\" id='" + dat.REC[i].GRP_CD + "'><span class=\"bg\"></span>";
            html += "\n        <div class=\"line " + firstDD + "\" style=\"margin:0 0 0 13px;\">";
            html += "\n                <span class=\"icon mygroup_icon" + dat.REC[i].GRP_ICON + "\"></span>";
            html += "\n            <a class=\"txt\" href=\"#none\" val-grpcd=\"" + dat.REC[i].GRP_CD + "\" val-grpnm=\"" + dat.REC[i].GRP_NM + "\" val-grpicon=\"" + dat.REC[i].GRP_ICON + "\">";
            html += "\n                <span title=\"" + dat.REC[i].GRP_NM + "\" style=\"width:115px;height:30px;line-height:30px;margin-top:-10px;overflow:hidden;text-overflow:ellipsis;-o-text-overflow:ellipsis;white-space:nowrap;display:block;\">" + dat.REC[i].GRP_NM + "</span>";
            html += "\n            </a>";
            html += "\n                <span class=\"count2 badge2\"><strong>" + dat.REC[i].GRP_CNT + "</strong></span>";
            html += "\n        </div>";
            html += "\n        <!-- layer -->";
            html += "\n        <div class=\"mygroup_layer\" style=\"display:none;\">";
            html += "\n            <div class=\"mygroup_layer_po\">";
            html += "\n                <span class=\"tail\"></span>";
            html += "\n                <ul>";
            html += "\n                    <li i18nCd=\"lang_renameGrp\">" + $.i18n.prop("lang_renameGrp") + "</li>";
            html += "\n                    <li class=\"on\" i18nCd=\"lang_emptyGrp\">" + $.i18n.prop("lang_emptyGrp") + "</li>";
            html += "\n                    <li i18nCd=\"lang_changeIcon\">" + $.i18n.prop("lang_changeIcon") + "</li>";
            html += "\n                    <li i18nCd=\"lang_deleteGrp\">" + $.i18n.prop("lang_deleteGrp") + "</li>";
            html += "\n                </ul>";
            html += "\n            </div>";
            html += "\n        </div>";
            html += "\n    </dd>";
        }
        var html2 = "";
        for (var i = 0; i < dat.REC.length; i++) {
            html2 += "    <li style=\"width:100px;overflow:hidden;text-overflow:ellipsis;-o-text-overflow:ellipsis;white-space:nowrap;display:block;\"><a href=\"#none\" val-grpcd=\"" + dat.REC[i].GRP_CD + "\"><img src=\"../img/btn/btn_checkbox3.gif\" alt=\"\" /></a> " + dat.REC[i].GRP_NM + "</li>";
        }
        $("#userGrpList").html(html); //화면 왼쪽 사용자그룹목록 표시 
        $("#userGrpSetList").html(html2); //그룹지정 > 사용자그룹목록 표시
        if (html2 == "") {
            $("#userGrpSetList").hide();
        }
        else {
            $("#userGrpSetList").show();
        }
        //그룹 순서 변경하기 
        $("#userGrpList").sortable({
            axis: "y",
            cursor: "move",
            update: function (event, ui) {
                var grpObj = $("#userGrpList dd");
                var sendGrpList = [];
                $.each(grpObj, function (i, v) {
                    sendGrpList.push({ "GRP_CD": $(v).attr("id") });
                });
                var jexAjax = jex.createAjaxUtil("empl_3006_01_u002");
                jexAjax.set("GRP_LIST", sendGrpList);
                jexAjax.execute(function (data) {
                    testHtml2();
                });
            }
        }); //.disableSelection();
        //		//왼쪽 메뉴 사용자그룹 선택시 
        //		$("#userGrpList dd a").bind("click", function(){
        //			
        //			_tmpCurGrpNm = $(this).find("span").html();
        //			$("#contentTitle h1").html(_tmpCurGrpNm);
        //		
        //			closeIfrContent2();
        //			leftMenuAllOff();
        //			$(this).parent().addClass("on");
        //
        //			_searchList.setInqyDsnc("3");
        //			_searchList.setPageNo(1);
        //			_searchList.setGrpCd($(this).attr("val-grpcd"));
        //			_searchList.setSrchWord("");
        //			parent.setSearchTextDefault();
        //
        //			drawTrcoList();				//직원 목록 표시
        //			drawBscGrpList();			//기본그룹 목록 표시
        //			drawUserGrpList();			//사용자그룹 목록 표시		
        //		});
        //왼쪽 메뉴 수정 아이콘
        $("#userGrpList dd .count2").bind("click", function () {
            $(this).parent().parent().find(".mygroup_layer").fadeIn();
        });
        $("#userGrpList dd").bind("mouseleave", function () {
            $(this).find(".mygroup_layer").hide();
        });
        $("#userGrpList dd").each(function () {
            var _this = $(this);
            var grpcd = $(this).find("a:eq(0)").attr("val-grpcd");
            var grpnm = $(this).find("a:eq(0)").attr("val-grpnm");
            var grpicon = $(this).find("a:eq(0)").attr("val-grpicon");
            $(this).find("ul li").each(function (idx) {
                $(this).click(function () {
                    //const grpnm = $(this).find("a:eq(0)").attr("val-grpnm");
                    _tmpCurGrpCd = grpcd;
                    _tmpCurGrpNm = grpnm;
                    _tmpCurGrpIcon = grpicon;
                    $(".layer_popup_wrap").each(function () {
                        $(this).hide(); //기존 레이어팝업창이 있으면 닫기
                    });
                    $("#userGrpList dd").each(function () {
                        $(this).show(); //기존에 그룹숨김항목은 표시처리
                    });
                    $("#userGrpList dd .new_group").each(function () {
                        $(this).parent().remove(); //기존에 그룹명변경 입력창 있으면 삭제하기
                    });
                    $(".new_group").hide(); //기존에 그룹추가 입력창 있으면 닫기처리
                    //const _myframe = $(document).find("iframe").contents();
                    //그룹명 수정
                    if (idx == 0) {
                        var sInputRenameDD = "";
                        sInputRenameDD += "\n    <!-- 그룹명변경 -->";
                        sInputRenameDD += "\n    <dd><span class=\"bg\"></span>";
                        sInputRenameDD += "\n        <div class=\"line\" style=\"display:none;\"><span class=\"icon mygroup_icon3_1\"></span>특별관리icon3 <span class=\"count2\"><em>12,500</em></span></div>";
                        sInputRenameDD += "\n        <!-- 새그룹 -->";
                        sInputRenameDD += "\n        <div class=\"new_group\">";
                        sInputRenameDD += "\n            <input type=\"text\" value=\"" + _tmpCurGrpNm + "\"><a href=\"#none\"><img src=\"../img/btn/btn_new_group_ok.gif\" alt=\"확인\"></a><a href=\"#none\"><img src=\"../img/btn/btn_new_group_cnl.gif\" alt=\"취소\"></a>";
                        sInputRenameDD += "\n        </div>";
                        sInputRenameDD += "\n        <!-- //새그룹 -->";
                        sInputRenameDD += "\n    </dd>";
                        sInputRenameDD += "\n    <!-- //그룹명변경 -->";
                        $(this).parent().parent().parent().parent().after($(sInputRenameDD));
                        $(_this).hide();
                        $("#userGrpList dd .new_group input").focus();
                    }
                    //그룹 비우기
                    else if (idx == 1) {
                        $("#empty").bPopup();
                        $("#empty").find("strong").html("'" + _tmpCurGrpNm + "'");
                        //_myframe.find("div#empty").html("_tmpCurGrpNm");
                        //$(".layer_popup_wrap:eq(1)").find("strong").text(_tmpCurGrpNm);
                        //$(".layer_popup_wrap:eq(1)").fadeIn();
                    }
                    //그룹 아이콘변경
                    else if (idx == 2) {
                        //						$(".layer_popup_wrap:eq(0)").fadeIn();
                        $("#layer_popup_wrapIcon").bPopup(slideDown);
                        var grpIconNo = _tmpCurGrpIcon.split("_")[0];
                        var grpColorNo = _tmpCurGrpIcon.split("_")[1];
                        $(".icon_select_wrap ul li").each(function () {
                            $(this).removeClass("on");
                        });
                        $(".color_select_wrap ul li").each(function () {
                            $(this).removeClass("on");
                        });
                        $(".icon_select_wrap ul li:eq(" + (grpIconNo - 1) + ")").addClass("on");
                        $(".color_select_wrap ul li:eq(" + (grpColorNo - 1) + ")").addClass("on");
                    }
                    //그룹 삭제
                    else if (idx == 3) {
                        //if($("#userGrpList dd").length == 1){
                        //	parent.showPrintInfoMsg("WE0031");//그룹을 삭제할 수 없습니다. 나의그룹은 1개 이상 있어야 합니다.
                        //}else{
                        $(".layer_popup_wrap:eq(2)").find("strong").html("'" + _tmpCurGrpNm + "'");
                        //							$(".layer_popup_wrap:eq(2)").fadeIn();
                        $("#delete").bPopup(slideDown);
                        $("#delete").find("strong").html("'" + _tmpCurGrpNm + "'");
                        //}
                    }
                });
            });
        });
        //그룹지정 체크박스 클릭시 
        $("#userGrpSetList li").bind("click", function () {
            var curChkImg = $(this).find("a img").attr("src");
            //const idxA = $("#userGrpSetList li a").parent().find("a").index(this);
            //체크박스 'V'상태면 '-' 으로 변경 (기본상태가 '-' 일경우)
            if (curChkImg.indexOf("btn_checkbox1.gif") > -1 && $(this).find("a").attr("val-checkstatus") == "2") {
                $(this).find("a img").attr("src", "../img/btn/btn_checkbox2.gif");
            }
            //체크박스 'V'상태면 'ㅁ'으로 변경 (기본상태가 '-'가 아닐경우)
            else if (curChkImg.indexOf("btn_checkbox1.gif") > -1 && $(this).find("a").attr("val-checkstatus") != "2") {
                $(this).find("a img").attr("src", "../img/btn/btn_checkbox3.gif");
            }
            //체크박스 '-'상태면 'ㅁ'으로 변경
            else if (curChkImg.indexOf("btn_checkbox2.gif") > -1) {
                $(this).find("a img").attr("src", "../img/btn/btn_checkbox3.gif");
            }
            //체크박스 'ㅁ'상태면 'V'으로 변경
            else if (curChkImg.indexOf("btn_checkbox3.gif") > -1) {
                $(this).find("a img").attr("src", "../img/btn/btn_checkbox1.gif");
            }
        });
    });
}
//그룹 선택표시 on/off
function leftMenuAllOff() {
    $("#totEmplBtn").removeClass("on"); //전체(회사명) 
    $("#userGrpDefault dt").removeClass("on"); //나의그룹(그룹전체) 
    $("#userGrpDefault dd").removeClass("on"); //나의그룹(즐겨찾기) 
    //기본그룹 선택표시 off 
    $("#bscGrpList dd").each(function () {
        $(this).removeClass("on");
    });
    //사용자그룹 선택표시 off
    $("#userGrpList dd").each(function () {
        $(this).removeClass("on");
    });
}
//즐겨찾기 등록/삭제
function updateBkmk(objA) {
    var emplidntid = $(objA).find("a").attr("val-emplidntid");
    var bkmkyn = $(objA).find("a").attr("val-bkmkyn");
    if (bkmkyn == "Y") {
        var jexAjax = jex.createAjaxUtil("empl_1002_05_d001");
        jexAjax.set("EMPL_IDNT_ID", emplidntid);
        jexAjax.execute(function (dat) {
            $(objA).find("a").attr("val-bkmkyn", "N");
            $(objA).find("a").find("img").attr("src", "../img/ico/icon_bookmark_off.png");
            $("#userBkmkCnt").html(parseInt($("#userBkmkCnt").html()) - 1);
            _SUBMIT_YN = "Y";
        });
    }
    else if (bkmkyn == "N") {
        var jexAjax = jex.createAjaxUtil("empl_1002_04_c001");
        jexAjax.set("EMPL_IDNT_ID", emplidntid);
        jexAjax.execute(function (dat) {
            $(objA).find("a").attr("val-bkmkyn", "Y");
            $(objA).find("a").find("img").attr("src", "../img/ico/icon_bookmark_on.png");
            $("#userBkmkCnt").html(parseInt($("#userBkmkCnt").html()) + 1);
            _SUBMIT_YN = "Y";
        });
    }
}
//휴지통 검색
function searchBasket() {
    //alert(viewType);
    //	_searchList.setInqyDsnc("4");
    _searchList.setPageNo(1);
    _searchList.setSrchWord("");
    _searchList.setPagePerCnt(10);
    //$(".list_table , .paging_wrap").show();
    $("#combo_row").text(_searchList.getPagePerCnt());
    $("#pagingPopBtn a:eq(0)").html("<span><em class=\"icon_list\" style=\"display:none;\"></em>" + _searchList.getPagePerCnt() + "</span>");
    parent.ifrMainResize("N", 30);
    parent.setSearchTextDefault();
    _INQ_JOIN_DV = "";
    if ($("#tab_list").length > 0) {
        $("#tab_list").hide(); //가입, 미가입 직원
    }
    //	_searchList.setPagePerCnt(10);
    //	$("#combo_row").text(_searchList.getPagePerCnt());
    //	$("#pagingPopBtn a:eq(0)").html("<span><em class=\"icon_list\" style=\"display:none;\"></em>"+_searchList.getPagePerCnt()+"</span>");
    //daravuth on 07.27.2016 
    excludeEmpl = true;
    drawTrcoList(0, 0); //직원 목록 표시
    drawBscGrpList(); //기본그룹 목록 표시
    drawUserGrpList(); //사용자그룹 목록 표시		
    leftMenuAllOff();
    $(".select_map a:eq(0)").removeClass("on").addClass("on");
    $(".select_map a:eq(1)").removeClass("on");
    $("#div_content").show();
    $("#ifr_content").hide();
    //daravuth 11.03.2016
    $(".view_listwrap").css("display", "none");
    $(".tab_schbx").css("display", "block");
}
function closeIfrContent() {
    _INQ_JOIN_DV = "";
    drawTrcoList(); //직원 목록 표시
    drawBscGrpList(); //기본그룹 목록 표시
    drawUserGrpList(); //사용자그룹 목록 표시		
    $("#div_content").show(); //직원목록
    $("#ifr_content").hide();
}
function closeIfrContent2() {
    $("#div_content").show();
    $("#ifr_content").hide();
}
function openIfrContent(url) {
    $("#ifr_content").attr("src", url);
    hideBnr();
    //	parent.ifrMainResize();
} //배너 hide 처리
function hideBnr() {
    $("#rcmd_bnr").hide();
}
//ani_yn : 스크롤 상단으로 조정할지 여부 : DEFAULT Y
function ifrContentResize(ani_yn) {
    $("#ifr_content").css("height", $("#ifr_content").contents().find("body").height() + "px");
    parent.ifrMainResize(ani_yn);
}
//ani_yn : 스크롤 상단으로 조정할지 여부 : DEFAULT Y, height1 추가할 픽셀
function ifrContentResize2(ani_yn, height1) {
    $("#ifr_content").css("height", ($("#ifr_content").contents().find("body").height() + height1) + "px");
    parent.ifrMainResize(ani_yn, height1);
}
function ifrContentResize(ani_yn, height1) {
    $("#ifr_content").css("height", ($("#ifr_content").contents().find("body").height() + height1) + "px");
    parent.ifrMainResize(ani_yn, height1);
}
function getViewID() {
    if ($("#div_content").css("display") != "none") {
        return "empl_1001_01";
    }
    return "ifr_content";
}
//앱연결 : 그린메시지 팝업 열기
function appGreenMeassagePop(userList) {
    var sizeW = 624;
    var sizeH = 543;
    var nLeft = screen.width / 2 - sizeW / 2;
    var nTop = screen.height / 2 - sizeH / 2;
    var option = ",toolbar=no,menubar=no,location=no,scrollbars=no,status=no,resizable=no";
    var userRec = {};
    userRec["USER_REC"] = userList;
    for (var i = 0; i < userList.length; i++) {
        if (userList[i].HP_NO.indexOf("*") > -1) {
            parent.showPrintInfoMsg("", "회사관리자를 설정하셔야만 이용 가능합니다. ");
            return;
        }
    }
    window.open('', 'appPop', "left=" + nLeft + ",top=" + nTop + ",width=" + sizeW + ",height=" + sizeH + option);
    $("#frm_appCall").remove();
    var strFormApp = "";
    strFormApp += "<form name=\"frm_appCall\" id=\"frm_appCall\" method=\"post\" target=\"appPop\">";
    strFormApp += "<input type=\"hidden\" name=\"SAUP_NO\" value=\"\"/>";
    strFormApp += "<input type=\"hidden\" name=\"PTL_USER_ID\" value=\"\"/>";
    strFormApp += "<input type=\"hidden\" name=\"CHNL_CODE\" value=\"0003\"/>";
    strFormApp += "<input type=\"hidden\" name=\"CHRG_GB\" value=\"0\"/>";
    strFormApp += "<input type=\"hidden\" name=\"PTL_ID\" value=\"\"/>";
    strFormApp += "<input type=\"hidden\" name=\"CHNL_ID\" value=\"\"/>";
    strFormApp += "<input type=\"hidden\" name=\"USE_INTT_ID\" value=\"\"/>";
    strFormApp += "<input type=\"hidden\" name=\"SAUP_NM\" value=\"\"/>";
    strFormApp += "<input type=\"hidden\" name=\"USER_REC\" value=\"\"/>";
    strFormApp += "</form>";
    $("body").append(strFormApp);
    var frm = $("#frm_appCall");
    $(frm).attr("action", $("#FORM_APP_GREEN_URL").val());
    //$(frm).attr("target", "appPop");
    $(frm).find("input[name=SAUP_NO]").val($("#FORM_SESSION_BSNN_NO").val());
    $(frm).find("input[name=PTL_USER_ID]").val($("#FORM_SESSION_USER_ID").val());
    $(frm).find("input[name=PTL_ID]").val($("#FORM_SESSION_PTL_ID").val());
    $(frm).find("input[name=CHNL_ID]").val($("#FORM_SESSION_CHNL_ID").val());
    $(frm).find("input[name=USE_INTT_ID]").val($("#FORM_SESSION_USE_INTT_ID").val());
    $(frm).find("input[name=SAUP_NM]").val($("#FORM_SESSION_BSNN_NM").val());
    $(frm).find("input[name=USER_REC]").val(JSON.stringify(userRec));
    //$(frm).find("input[name=USER_REC]").val(JSON.stringify(userRec).replace(/\"/g,"'"));
    $(frm).submit();
}
//앱연결 : 콜라보 팝업 열기
function appColaboPop(userList) {
    for (var i = 0; i < userList.length; i++) {
        var userid = null2void(userList[i].USER_ID, "");
        //const eml = null2void(userList[i].EML,"");
        var clph_no = null2void(userList[i].CLPH_NO, "");
        if (userid == "" && clph_no == "") {
            //parent.showPrintInfoMsg("","포탈사용자가 아니거나 이메일 정보가 없는 직원은 콜라보를 보낼 수 없습니다.");
            parent.showPrintInfoMsg("", "포탈사용자가 아니거나 휴대폰 번호가 없는 직원은 콜라보를 보낼 수 없습니다.");
            return;
        }
    }
    var sizeW = 800;
    var sizeH = 700;
    var nLeft = screen.width / 2 - sizeW / 2;
    var nTop = screen.height / 2 - sizeH / 2;
    var option = ",toolbar=no,menubar=no,location=no,scrollbars=no,status=no,resizable=no";
    $("#frm_appCall").remove();
    var strFormApp = "";
    strFormApp += "<form name=\"frm_appCall\" id=\"frm_appCall\" method=\"post\" target=\"appPop\">";
    strFormApp += "<input type=\"hidden\" name=\"PTL_ID\" value=\"\"/>";
    strFormApp += "<input type=\"hidden\" name=\"CHNL_ID\" value=\"\"/>";
    strFormApp += "<input type=\"hidden\" name=\"USE_INTT_ID\" value=\"\"/>";
    strFormApp += "<input type=\"hidden\" name=\"USER_ID\" value=\"\"/>";
    strFormApp += "<input type=\"hidden\" name=\"USER_NM\" value=\"\"/>";
    strFormApp += "<input type=\"hidden\" name=\"CORP_NM\" value=\"\"/>";
    strFormApp += "<input type=\"hidden\" name=\"DVSN_NM\" value=\"\"/>";
    strFormApp += "<input type=\"hidden\" name=\"CLPH_NO\" value=\"\"/>";
    strFormApp += "<input type=\"hidden\" name=\"CLPH_NTL_CD\" value=\"\"/>";
    strFormApp += "<input type=\"hidden\" name=\"TTL\" value=\"\"/>";
    strFormApp += "<input type=\"hidden\" name=\"CNTN\" value=\"\"/>";
    strFormApp += "<input type=\"hidden\" name=\"RCVR_REC\" value=\"\"/>";
    strFormApp += "</form>";
    $("body").append(strFormApp);
    sf_setFrom();
    //송신자정보조회
    function sf_setFrom() {
        var jexAjax = jex.createAjaxUtil("empl_9002_01_r001");
        jexAjax.set("PTL_ID", $("#FORM_SESSION_PTL_ID").val());
        jexAjax.set("USER_ID", $("#FORM_SESSION_USER_ID").val());
        jexAjax.execute(function (dat) {
            var frm = $("#frm_appCall");
            $(frm).find("input[name=PTL_ID]").val($("#FORM_SESSION_PTL_ID").val());
            $(frm).find("input[name=CHNL_ID]").val($("#FORM_SESSION_CHNL_ID").val());
            $(frm).find("input[name=USE_INTT_ID]").val($("#FORM_SESSION_USE_INTT_ID").val());
            $(frm).find("input[name=USER_ID]").val($("#FORM_SESSION_USER_ID").val());
            $(frm).find("input[name=USER_NM]").val(dat.FLNM);
            $(frm).find("input[name=CORP_NM]").val(dat.BSNN_NM);
            $(frm).find("input[name=DVSN_NM]").val(null2void(dat.DVSN_NM, ""));
            $(frm).find("input[name=CLPH_NO]").val(null2void(dat.CLPH_NO, ""));
            $(frm).find("input[name=CLPH_NTL_CD]").val(null2void(dat.CLPH_NTNL_CD, ""));
            sf_setTo();
        });
    }
    //수신자정보조회
    function sf_setTo() {
        var sendUserList = [];
        for (var i = 0; i < userList.length; i++) {
            sendUserList.push({
                //"OUT_RCVR_EMAIL": userList[i].EML,
                "RCVR_CLPH_NO": userList[i].CLPH_NO,
                "RCVR_CLPH_NTL_CD": userList[i].CLPH_NTNL_CD,
                "RCVR_USER_NM": userList[i].FLNM,
                //"IN_RCVR_USE_INTT_ID": $("#FORM_SESSION_USE_INTT_ID").val(),
                "RCVR_USER_ID": userList[i].USER_ID,
                //"RCVR_CORP_NM": $("#FORM_SESSION_BSNN_NM").val(),
                //"RCVR_DVSN_NM": userList[i].DVSN_NM,
                "REQ_CNTN": "001"
            });
        }
        var frm = $("#frm_appCall");
        $(frm).find("input[name=RCVR_REC]").val(JSON.stringify(sendUserList).replace(/\"/g, "'"));
        sf_setColabo();
    }
    //콜라보 팝업 열기
    function sf_setColabo() {
        var frm = $("#frm_appCall");
        $(frm).attr("action", $("#FORM_APP_COLA_URL").val());
        var objPopup = window.open('', 'appPop', "left=" + nLeft + ",top=" + nTop + ",width=" + sizeW + ",height=" + sizeH + option);
        if (objPopup == null) {
            alert("팝업차단 기능을 해지 하여 주십시요.");
            return;
        }
        //$(frm).attr("target", "appPop");
        $(frm).submit();
    }
}
// 일정 앱 호출
function appSchedulePop(userList) {
    var SCHD_PTL_ID = $("#FORM_SESSION_PTL_ID").val();
    var SCHD_CHNL_ID = $("#FORM_SESSION_CHNL_ID").val();
    var SCHD_USE_INTT_ID = $("#FORM_SESSION_USE_INTT_ID").val();
    var SCHD_USER_ID = $("#FORM_SESSION_USER_ID").val();
    var SCHD_CNTS_CRTC_KEY = $("#SCHD_KEY").val();
    fn_setSchdVar();
    function fn_setSchdVar() {
        var s_shrn_list = "";
        s_shrn_list += "<input type='hidden' name='PTL_ID' value='" + SCHD_PTL_ID + "'/>";
        s_shrn_list += "<input type='hidden' name='CHNL_ID' value='" + SCHD_CHNL_ID + "'/>";
        s_shrn_list += "<input type='hidden' name='USE_INTT_ID' value='" + SCHD_USE_INTT_ID + "'/>";
        s_shrn_list += "<input type='hidden' name='USER_ID' value='" + SCHD_USER_ID + "'/>";
        s_shrn_list += "<input type='hidden' name='GUBUN' value='I'/>"; //GUBUN - I 등록 / U - 수정 
        s_shrn_list += "<input type='hidden' name='TTL' value=''/>";
        s_shrn_list += "<input type='hidden' name='CNTN' value=''/>";
        s_shrn_list += "<input type='hidden' name='CNTS_CRTC_KEY' value='" + SCHD_CNTS_CRTC_KEY + "'/>";
        //		REL_IDNT_GB : U:사용자ID, E:직원식별ID, C:연락처일련번호, CP:휴대폰, ED:부서코드, EG:직원그룹코드, CG:연락처그룹코드
        //		GRP_CD : REL_IDNT_GB의 값
        //		GRP_NM : 명칭
        var jsonShrnData = [];
        for (var i = 0; i < userList.length; i++) {
            jsonShrnData.push({
                "REL_IDNT_GB": "CP",
                "GRP_CD": userList[i].CLPH_NTNL_CD + "-" + userList[i].CLPH_NO,
                "GRP_NM": userList[i].FLNM
            });
        }
        s_shrn_list += "<input type='hidden' name='SHRN_REC' value='" + encodeURIComponent(JSON.stringify(jsonShrnData)) + "'/>";
        fn_setSchdFrm(s_shrn_list);
    }
    /*
     * 일정 form 세팅
    */
    function fn_setSchdFrm(s_shrn_list) {
        var $frmObj = $("<form id='schd_form' name='schd_form'></form>'");
        $frmObj.attr("method", "post");
        $frmObj.appendTo("body");
        $frmObj.append($(s_shrn_list));
        fn_setSchdPopup($frmObj, 'SCHD_AP_C001.act');
    }
    /*
     * 일정 팝업
     */
    function fn_setSchdPopup(frm, page) {
        /* ============= 구분해줘야함.. =============== */
        if (isReal(_domain)) {
            action = "http://calendar.appplay.co.kr/"; //real
        }
        else {
            action = "http://calendar.webcashcorp.com:82/"; //dev
        }
        /* ============= 구분해줘야함.. =============== */
        action += page;
        var sizeW = 650;
        var sizeH = 455;
        var nLeft = screen.width / 2 - sizeW / 2;
        var nTop = screen.height / 2 - sizeH / 2;
        var option = ",toolbar=no,menubar=no,location=no,scrollbars=no,status=no,resizable=no";
        frm.attr("action", action);
        var objPopup = window.open('', 'appPop', "left=" + nLeft + ",top=" + nTop + ",width=" + sizeW + ",height=" + sizeH + option);
        if (objPopup == null) {
            alert("팝업차단 기능을 해지 하여 주십시요.");
            return;
        }
        frm.attr("target", "appPop");
        frm.submit();
        frm.remove();
    }
}
function fn_openerCall() {
    if (opener) {
        if (typeof (opener.fn_schdCallBack) != "undefined") {
            alert("등록되었습니다.");
            self.close();
        }
        else {
            alert("Callback 함수가 없습니다. 관리자에게 문의하십시오.");
        }
    }
}
/**
 * <pre>
 * 해외 전화번호 형식 반환
 * </pre>
 * @param 전화번호, 국가코드
 * */
function getWorldNo(phone_val, world_cd) {
    var rtn_no = "";
    if (phone_val == "")
        return rtn_no;
    world_cd = null2void(world_cd, "");
    if (world_cd == $("#KOR_DEF_CD").val() || world_cd == "") {
        //$("#user_phone").html(getTel(null2void(dat.EXNM_NO, "")));
        rtn_no = "+82" + getTel(null2void(phone_val, "").replace(/-/gi, "")).replace(/^0+/, " ").replace(/\-/g, " ");
        //rtn_no = getTel(null2void(phone_val, "").replace(/-/gi, ""));
    }
    else {
        //$("#user_phone").html(null2void(dat.EXNM_NO, "-"));
        if (world_cd.split("_").length > 1) {
            rtn_no = "+" + world_cd.split("_")[1] + "" + null2void(phone_val, " ").replace(/-/gi, "");
        }
    }
    return rtn_no;
}
//virak (2015_01_12)
//직원 초대 팝업
//isAuto : 배너일 경우 true(전체선택), 아닐 경우 false
//obj : 미가입직원 하트선택일 경우 넘기는 td 오브젝트
function getSelectInfo(isAuto, obj) {
    if ($("#str_cnts_msgr_yn").attr("value") == "N") {
        return;
    }
    var rec = [];
    var ischeck = false;
    /*const str_ptl_id=$("#PTL_ID", window.parent.document).val();
    const str_chnl_id=$("#CHNL_ID",window.parent.document).val();
    const str_use_intt_id=$("#USE_INTT_ID",window.parent.document).val();
    const str_user_id=$("#USER_ID",window.parent.document).val();*/
    var str_ptl_id = $("#FORM_SESSION_PTL_ID").val();
    var str_chnl_id = $("#FORM_SESSION_CHNL_ID").val();
    var str_use_intt_id = $("#FORM_SESSION_USE_INTT_ID").val();
    var str_user_id = $("#FORM_SESSION_USER_ID").val();
    var str_cctn_chnl_id = $("#FORM_SESSION_CCTN_CHNL_ID").val();
    var heartName = "";
    if (obj != undefined && null2void(obj, "") != "") { //미가입 직원 하트선택
        var dat = { EMPL_IDNT_ID: "", FLNM: "", CLPH_NO: "", CLPH_NO_NTNL_CD: "", BSNN_NM: "" };
        dat.EMPL_IDNT_ID = $(obj).parent().find("input[name='EMPL_IDNT_ID']").val();
        dat.FLNM = $(obj).parent().find("input[name='FLNM']").val();
        dat.EML = $(obj).parent().find("input[name='EML']").val();
        dat.CLPH_NO = $(obj).parent().find("input[name='CLPH_NO']").val();
        dat.CLPH_NTNL_CD = $(obj).parent().find("input[name='CLPH_NTNL_CD']").val();
        dat.BSNN_NM = $(obj).parent().find("input[name='BSNN_NM']").val();
        rec.push(dat);
    }
    else {
        $("#tb_TRCO_LIST tbody tr").each(function () {
            ischeck = $(this).find("td:eq(0) input[name=trco]").is(":checked");
            heartName = $(this).find("img[attr_nm='heart_img']").attr("src");
            //if(ischeck && heartName=="../img/ico/icon_disuse.png"){
            if (ischeck && heartName.indexOf("img/ico/icon_disuse.png") > -1) {
                var dat = { EMPL_IDNT_ID: "", FLNM: "", CLPH_NO: "", CLPH_NO_NTNL_CD: "", BSNN_NM: "" };
                dat.EMPL_IDNT_ID = $(this).find("td:eq(0) input[name=EMPL_IDNT_ID]").val();
                dat.FLNM = $(this).find("td:eq(0) input[name='FLNM']").val();
                dat.EML = $(this).find("td:eq(0) input[name='EML']").val();
                dat.CLPH_NO = $(this).find("td:eq(0) input[name='CLPH_NO']").val();
                dat.CLPH_NTNL_CD = $(this).find("td:eq(0) input[name='CLPH_NTNL_CD']").val();
                dat.BSNN_NM = $(this).find("td:eq(0) input[name='BSNN_NM']").val();
                rec.push(dat);
            }
        });
    }
    //const url=_domain+"/emplinfo_0002_01.act?PTL_ID="+str_ptl_id+"&CHNL_ID="+str_chnl_id+"&USE_INTT_ID="+str_use_intt_id+"&USER_ID="+str_user_id+"&SECR_KEY="+$("#SECR_KEY").val()+"&useUnicode=true&characterEncoding=UTF-8&auto="+isAuto;
    var url = _domain + "/emplinfo_0002_01.act?PTL_ID=" + str_ptl_id + "&CHNL_ID=" + str_chnl_id + "&USE_INTT_ID=" + str_use_intt_id + "&USER_ID=" + str_user_id + "&SECR_KEY=" + $("#SECR_KEY").val() + "&useUnicode=true&characterEncoding=UTF-8&auto=" + isAuto + "&CCTN_CHNL_ID=" + str_cctn_chnl_id;
    //postPop("","",482,320);
    parent.openEmplPopContent2(url, 482, 522);
    $("#REC").val(JSON.stringify(rec));
    var frm = document.frm;
    frm.method = "post";
    frm.action = url;
    frm.target = "ifr_sub";
    frm.submit();
}
//virak(2015-01-22)
//function()
var slideDown = {
    easing: 'easeOutBack',
    speed: 450,
    transition: 'slideDown'
};
function ifr_frMainResize(ani_yn, height1) {
    if (typeof (height1) != "number") {
        height1 = 0;
    }
    $("#ifr_content").css("height", ($("#ifr_content").contents().height() + height1) + "px");
    if (ani_yn != "N") { //스크롤 조정 여부
        $('body,html').animate({
            scrollTop: 0
        }, 0);
    }
}
;
//not uesed
function loadDefaultConfig() {
    var jexAjax = jex.createAjaxUtil("empl_4001_01_r001");
    jexAjax.setErrTrx(false);
    jexAjax.execute(function (dat) {
        var VIEW_TYPE = dat.VIEW_TYPE;
        if (VIEW_TYPE == 1) {
            $(".btnbx_r a.btn_thumb").click();
        }
        else if (VIEW_TYPE == 2) {
            $(".btnbx_r a.btn_list").click();
        }
        else if (VIEW_TYPE == 3) {
            $(".btnbx_r a.btn_org").click();
        }
    });
}
function checkViewType(dvsn_cd) {
    if ($(".btnbx_r a.btn_org").hasClass("on") == true || dvsn_cd == null || dvsn_cd == "") {
        orgBusinessObj.selectNode(dvsn_cd);
    }
    else {
    }
}
function detectIE() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }
    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }
    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }
    // other browser
    return false;
}
//20-20-2016 completed
var testload = 1;
var offset = 0;
function ckScreenSize(curObj) {
    var screenHeight = parseInt(screen.height);
    var contentHeight = parseInt(parent.$('#ifr_main').height()); //parseInt(parent.$('#ifr_main').contents().find(".thumb_viewwrap").height());//
    var diffIfmSizeMain = parseInt(parent.$('#ifr_main').contents().find(".thumb_viewwrap").height());
    var diffIfmSizeMainList = parseInt(parent.$('#ifr_main').contents().find("#tb_TRCO_LIST").height());
    var diffIfmSizeMainTree = parseInt(parent.$('#ifr_main').contents().find("#myOrgArea").height());
    var content_wrap = parseInt($('.content_wrap').height());
    var view_type = $("#VIEW_TYPE_CD").val();
    var offset = screenHeight - contentHeight;
    offset = offset - content_wrap / 4;
    switch (view_type) {
        case '1':
            if (testload <= 2) {
                testload++;
                $("#div_content").height() + 20;
                parent.ifrMainResize("N", 0);
                //offset = contentHeight - screenHeight;
            }
            else {
                if (diffIfmSizeMain <= contentHeight) {
                    if (contentHeight <= screenHeight) {
                        $("#div_content").height(screenHeight - offset); //dynamic by offset??
                        parent.ifrMainResize("N", 0);
                        if (testload == 10) {
                            testload = 2;
                        }
                    }
                    else {
                        $("#div_content").css("height", "auto");
                        parent.ifrMainResize("N", 0);
                        if (testload == 10) {
                            testload = 2;
                        }
                    }
                }
                else {
                    if (diffIfmSizeMain <= contentHeight) {
                        $("#div_content").height(screenHeight - offset);
                        parent.ifrMainResize("N", 0);
                        if (testload == 10) {
                            testload = 2;
                        }
                    }
                    else {
                        $("#div_content").css("height", "auto");
                        parent.ifrMainResize("N", 0);
                        if (testload == 10) {
                            testload = 2;
                        }
                    }
                }
            }
            break;
        case '2':
            if (testload <= 2) {
                testload++;
                $("#div_content").height(screenHeight - 82); //dynamic by offset??
                parent.ifrMainResize("N", 0);
                offset = contentHeight - screenHeight;
            }
            else {
                if (diffIfmSizeMainList <= contentHeight) {
                    if (contentHeight <= screenHeight) {
                        $("#div_content").height(screenHeight - offset); //dynamic by offset??
                        parent.ifrMainResize("N", 0);
                        if (testload == 10) {
                            testload = 2;
                        }
                    }
                    else {
                        $("#div_content").css("height", "auto");
                        parent.ifrMainResize("N", 0);
                        if (testload == 10) {
                            testload = 2;
                        }
                    }
                }
                else {
                    if (diffIfmSizeMainList <= contentHeight) {
                        $("#div_content").height(screenHeight - offset);
                        parent.ifrMainResize("N", 0);
                        if (testload == 10) {
                            testload = 2;
                        }
                    }
                    else {
                        $("#div_content").css("height", "auto");
                        parent.ifrMainResize("N", 0);
                        if (testload == 10) {
                            testload = 2;
                        }
                    }
                }
            }
            break;
        case '3':
            if (testload <= 2) {
                testload++;
                parent.ifrMainResize("N", 0);
                offset = contentHeight - screenHeight;
            }
            else {
                if (diffIfmSizeMainTree <= contentHeight) {
                    if (contentHeight <= screenHeight) {
                        $("#div_content").height(screenHeight - offset); //dynamic by offset??
                        parent.ifrMainResize("N", 0);
                        if (testload == 10) {
                            testload = 2;
                        }
                    }
                    else {
                        $("#div_content").css("height", "auto");
                        parent.ifrMainResize("N", 0);
                        if (testload == 10) {
                            testload = 2;
                        }
                    }
                }
                else {
                    if (diffIfmSizeMainTree <= contentHeight) {
                        $("#div_content").height(screenHeight - offset);
                        parent.ifrMainResize("N", 0);
                        if (testload == 10) {
                            testload = 2;
                        }
                    }
                    else {
                        $("#div_content").css("height", "auto");
                        parent.ifrMainResize("N", 0);
                        if (testload == 10) {
                            testload = 2;
                        }
                    }
                }
            }
            break;
        default:
    }
}
/*function ckIframSize(currObj){
    const screenHeight 		= parseInt(screen.height);
    const contentHeight 		= parseInt($('#ifr_content').height());
    const content_wrap 		= parseInt($('.content_wrap').height());
    const offset				= screenHeight - contentHeight;
    offset	= offset - content_wrap/4;
    //console.log("offset : " , offset);
    if(contentHeight <= screenHeight){
        parent.$("#ifr_main").height(screenHeight);//.content_wrap
        $("#ifr_content").height(screenHeight);//.content_wrap
        parent.ifrMainResize("N", 0);
        
    }
}*/
function ckIframSize(currObj) {
    var screenHeight = parseInt(screen.height);
    var contentHeight = parseInt($('#ifr_content').height());
    var content_wrap = parseInt($('.content_wrap').height());
    var offset = screenHeight - contentHeight;
    offset = offset - content_wrap / 4;
    if (contentHeight <= screenHeight) {
        $("#ifr_content").height(screenHeight - offset); //.content_wrap
        //;console.log($("#ifr_content").height());
        parent.ifrMainResize("N", 40);
    }
    parent.ifrMainResize("N", 40);
}
function addTabBold(obj) {
    $("#list_cnt_all").parent("li").removeClass("on");
    $("#list_trash").parent("li").removeClass("on");
    $("#list_stop_termination").parent("li").removeClass("on");
    $(obj).parent("li").addClass("on");
}
function excel_download() {
    _loding.start();
    var jexAjax = jex.createAjaxUtil("empl_1001_01_l001");
    //	jexAjax.set("INQY_DSNC", 	1);
    jexAjax.set("INQY_DSNC", _searchList.getInqyDsnc());
    jexAjax.set("GRP_CD", _searchList.getGrpCd());
    jexAjax.set("SRCH_WORD", _searchList.getSrchWord());
    jexAjax.set("BSNN_NO", _searchList.getBsnnNo());
    jexAjax.set("JOIN_YN", _INQ_JOIN_DV); //가입 조회조건
    jexAjax.setErrTrx(false);
    jexAjax.execute(function (dat) {
        if (dat.COMMON_HEAD.ERROR) {
            top.showPrintInfoMsg(dat.COMMON_HEAD.MESSAGE);
            _loding.stop();
            return;
        }
        else {
            var headerName = [];
            var headerId = [];
            var headerCnt = dat.VIEW_REC.length;
            for (var i = 0; i < dat.VIEW_REC.length; i++) {
                headerName.push(dat.VIEW_REC[i].KORN_CLMN_NM);
                headerId.push(dat.VIEW_REC[i].CLMN_NM);
            }
            var _grid;
            //Set header
            var header = [];
            for (var k = 0; k < headerCnt; k++) {
                header.push({
                    key: headerId[k],
                    name: headerName[k],
                    width: 80,
                    colClass: ''
                });
            }
            _grid = makeGrid(header);
            //Set data
            $.map(dat.EMPL_REC, function (val) {
                val["OOH_POST_ADRS"] = null2void(val["OOH_POST_ADRS"], "") + " " + null2void(val["OOH_DTL_ADRS"], "");
                val["DTPL_POST_ADRS"] = null2void(val["DTPL_POST_ADRS"], "") + " " + null2void(val["DTPL_DTL_ADRS"], "");
                val["LNR_SLCN"] = (null2void(val["LNR_SLCN"], "") == "1" ? "음력" : (null2void(val["LNR_SLCN"], "") == "2" ? "양력" : ""));
                val["WDNG_YN"] = (null2void(val["WDNG_YN"], "") == "Y" ? "기혼" : (null2void(val["WDNG_YN"], "") == "N" ? "미혼" : ""));
                var _tel_no = getWorldNo(null2void(val["EXNM_NO"]));
                if ($("#NTCD_RPSN_YN").val() == "N")
                    _tel_no = getTel(null2void(val["EXNM_NO"]));
                var _exnm_no = null2void(val["CMPN_TLPH_NO"]);
                if (_exnm_no != null && _exnm_no.length > 0) {
                    _exnm_no = " (" + _exnm_no + ")";
                    _exnm_no = _tel_no + _exnm_no;
                }
                else {
                    _exnm_no = _tel_no;
                }
                ;
                val["EXNM_NO"] = _exnm_no;
                var _clph_no = getWorldNo(null2void(val["CLPH_NO"], ""), null2void(val["CLPH_NTNL_CD"], ""));
                if ($("#NTCD_RPSN_YN").val() == "N")
                    _clph_no = getTel(null2void(val["CLPH_NO"]));
                val["CLPH_NO"] = _clph_no;
                var _ootl_no = getWorldNo(null2void(val["OOTL_NO"], ""), null2void(val["OOTL_NO_NTNL_CD"], ""));
                if ($("#NTCD_RPSN_YN").val() == "N")
                    _ootl_no = getTel(null2void(val["OOTL_NO"]));
                val["OOTL_NO"] = _ootl_no;
                return val;
            });
            _grid.dataMgr.set(dat.EMPL_REC);
            excelDownload(_grid, "직원 리스트", "N");
            _loding.stop();
        }
    });
}
;
$(document).ready(function () {
    try {
        var excelDownloadHtml = "<div style='display:none' id='_jexGridExcelDown'>";
        excelDownloadHtml += "<form method='post' enctype='multipart/form-data' name='_jexGridDownloadForm' id='_jexGridDownloadForm' action='/emplinfo/comm/excel_download_view.jsp' target='_jexGridDownloadIfrm'>";
        excelDownloadHtml += "<textarea  name='json' id='json'></textarea>";
        excelDownloadHtml += "</form>";
        excelDownloadHtml += "<iframe name='_jexGridDownloadIfrm' id='_jexGridDownloadIfrm' style='width:0px;height:0px;display:none'/>";
        excelDownloadHtml += "</div>";
        $("body").append(excelDownloadHtml);
    }
    catch (e) { }
    ;
});
function makeGrid(header) {
    //그리드 옵션 설정
    //const gridOptions = grid.getDefaultScardOption();
    $("body").append("<div id='grid' style='display:none'></div>");
    //Jex Grid Column
    var _grid = JGM.create("Grid", { container: document.getElementById("grid"), colDefs: header });
    return _grid;
}
;
function excelDownload(downGrid, title, showDateYn) {
    showDateYn = jex.null2Void(showDateYn, "Y");
    //원본 그리드 정보읽기
    var orgGrid = downGrid;
    var jgridDownload = {};
    jgridDownload.fileTitle = {
        title: jex.null2Void(title, ""),
        details: []
    };
    //파일의 헤더(타이틀)정보 읽기
    if (!!orgGrid.fileTitle) {
        jgridDownload.fileTitle.title = orgGrid.fileTitle.title;
        for (var key in orgGrid.fileTitle.details) {
            if (!!orgGrid.fileTitle.details[key].key) {
                jgridDownload.fileTitle.details.push({
                    key: orgGrid.fileTitle.details[key].key,
                    value: orgGrid.fileTitle.details[key].value
                });
            }
        }
    }
    var _datalist = orgGrid.dataMgr.datalist;
    var _datalistLength = _datalist.length;
    if (_datalist.length == 0) {
        alert("저장 할 항목이 없습니다. 저장항목을 확인해주세요.");
        return false;
    }
    var orgColDef = orgGrid.colDefMgr.getAll();
    var _colDefList = [];
    for (var i = 0; i < orgColDef.length; i++) {
        if (orgColDef[i].key == "checkbox" || orgColDef[i].hidden) {
            continue;
        }
        _colDefList.push({
            gridunqid: String(i),
            name: orgColDef[i].name,
            key: orgColDef[i].key,
            width: orgColDef[i].width,
            sumRenderer: !!orgColDef[i].sumRenderer ? true : false,
            renderer: orgColDef[i].renderer,
            excelFormat: orgColDef[i].excelFormat
        });
    }
    var _colDefLength = _colDefList.length;
    var _saveDatalist = [];
    var _saveDatarow;
    var pattern = /(<([^>]+)>)/gi; // 컬럼안에 태그 있을경우 내용까지 공백으로 변환되어 변경 by lsh
    for (var i = 0; i < _datalistLength; i++) {
        _saveDatarow = {};
        for (var j = 0; j < _colDefLength; j++) {
            var _cellValue;
            //const _cellValue = !!_colDefList[j].renderer?_colDefList[j].renderer(_datalist[i][ _colDefList[j].key ] , i):_datalist[i][ _colDefList[j].key ];
            if (!!_colDefList[j].renderer) {
                try {
                    var tempValue = _datalist[i][_colDefList[j].key];
                    if (tempValue == null || tempValue == undefined || tempValue == "" || tempValue == "undefined") {
                        tempValue = "";
                    }
                    _cellValue = _colDefList[j].renderer(tempValue, i, j, _datalist[i], _colDefList[j]);
                    if (_cellValue == null || _cellValue == undefined || _cellValue == "" || _cellValue == "undefined") {
                        _cellValue = "";
                    }
                    else {
                        if (typeof _cellValue == "string") {
                            _cellValue = _cellValue.replace(pattern, "").replace(/&nbsp;/ig, " ");
                        }
                    }
                }
                catch (e) {
                    _cellValue = "";
                }
            }
            else {
                _cellValue = _datalist[i][_colDefList[j].key];
            }
            if (_colDefList[j].excelFormat == "int") {
                if ((typeof _cellValue) == "string") {
                    _cellValue = parseInt(_cellValue.replace(/,/g, "").replace("\\(", "").replace("\\)", ""));
                }
            }
            _saveDatarow["A" + j] = _cellValue == undefined ? "" : _cellValue;
        }
        _saveDatalist.push(_saveDatarow);
    }
    var result = {
        colDef: _colDefList,
        data: _saveDatalist,
        title: jgridDownload.fileTitle,
        showDate: showDateYn
    };
    $("#_jexGridDownloadForm").find("#json").val(encodeURI(JSON.stringify(result).replace(/\+/g, "%2B")));
    $("#_jexGridDownloadForm").submit();
}
;
function changeLang(lang) {
    jQuery.i18n.properties({
        name: 'empl_1001_01',
        path: '/lang/',
        mode: 'both',
        language: lang,
        async: true,
        callback: function () {
            emplinfo2.ui.setAllLang("body", $.i18n.prop);
            top.changeLang(lang);
            if (document.getElementById('ifr_content').contentWindow.changeLang) {
                document.getElementById('ifr_content').contentWindow.changeLang(lang);
            }
            if ($.i18n.prop("lang_unit") != "개") {
                $(".combo_title_add").css({ "width": "48px" });
                $(".combo_style ul").css({ "width": "70px" });
                $(".combo_layer_add").css({ "width": "70px" });
            }
            else {
                $(".combo_title_add").css({ "width": "42px" });
                $(".combo_style ul").css({ "width": "64px" });
                $(".combo_layer_add").css({ "width": "66px" });
            }
        }
    });
}
